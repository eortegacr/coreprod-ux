sliderInt=1;
sliderNext=2;

function startSlider(){
	count = $("#slides > Div").size();
	loop = setInterval(function(){
		if(sliderNext > count){
			sliderNext=1;
			sliderInt = 1;
		}
		$("#slides > div").hide( "slide", { direction: "left" }, 200 );
		$("#slides > div#slide" +sliderNext).show( "slide", { direction: "right" }, 200 );
		$('.pagination > a').removeClass('pagactive');
		$('.pagination > a:eq('+sliderInt+')').addClass('pagactive');
		sliderInt = sliderNext;
		sliderNext = sliderNext +1;
	}, 15000)
}

function stopLoop(){
	window.clearInterval(loop);
}

function showSlide(id){
	stopLoop();
	if(id > count){
		id=1;}
	else if(id < 1) {
		id = count;}
	$("#slides > div").fadeOut(200);
	$("#slides > div#slide" + id).fadeIn(200);
	$('.pagination > a').removeClass('pagactive');
	$('.pagination > a:eq('+(id-1)+')').addClass('pagactive');
	sliderInt = id;
	sliderNext = id +1;
	startSlider();
}

function prev(){
	newSlide = sliderInt - 1;
	showSlide(newSlide);
}

function next(){
	newSlide = sliderInt + 1;
	showSlide(newSlide);
}


$(document).ready(function(){

	$("#slides > div#slide1").fadeIn(200);
	$('.pagination > a').removeClass('pagactive');
	$('.pagination > a:eq(0)').addClass('pagactive');
	startSlider();

	$( ".pagination > a" ).click(function() {
		pag = $( ".pagination > a" ).index( this ) + 1;
		stopLoop();
		$("#slides > div").fadeOut(200);
		$("#slides > div#slide" + pag).fadeIn(200);
		$('.pagination > a').removeClass('pagactive');
		$(this).addClass('pagactive');
		startSlider();
		if(sliderNext =  pag){
			sliderNext=(pag+1);
		}
	});

	$("#slides > div").hover(
		function (){
			stopLoop();
		},
		function (){
			startSlider();
		}
	);
});

$(window).load(function(){

	$('.welcomeVideoBtn').click(function(){
		if($('.welcomeVideo').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.welcomeVideo').parent('.mainOverlay').slideDown(800);
			$('.welcomeVideo').fadeIn(1000);
			return false;
		}
	});

	$(".welcomeBtnRecog").on("click", function(e){
		e.preventDefault();
		$("a[class='recognize']").trigger("click");
		$('html,body').animate({
			scrollTop: $(".dashboard").offset().top - 70
		});
		$('.recog_EmpLookup').focus();
	});

	$(".welcomeBtnSocial").on("click", function(e){
		e.preventDefault();
		$("a[class='recfeed']").trigger("click");
		$('html,body').animate({
			scrollTop: $(".dashboard").offset().top - 70
		});
	});

	$(".updatesBtnCongrats").on("click", function(e){
		e.preventDefault();
		$("a[class='recfeed']").trigger("click");
		$('html,body').animate({
			scrollTop: $(".dashboard").offset().top - 70
		});
		$('.feed .post:first-of-type').find('textarea').focus();
	});

	$(".updatesBtnSurvey").on("click", function(e){
		e.preventDefault();
		$("a[class='survey']").trigger("click");
		$('html,body').animate({
			scrollTop: $("#dashboard").offset().top - 70
		});
		if($('.announcements').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
	});

	$(".updatesBtnNewRecog").on("click", function(e){
		e.preventDefault();
		$("a[class='myfeed']").trigger("click");
		$('html,body').animate({
			scrollTop: $("#dashboard").offset().top - 70
		});
		if($('.announcements').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
	});

	$('.updatesBtnMilestones').click(function(){
		var peerName= $(this).find('.milestoneEmpName').text();
		$('.peerName').empty();
		$('.peerName').text(peerName);
		if($('.milestoneCongrate').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.milestoneCongrate').parent('.mainOverlay').slideDown(800);
			$('.milestoneCongrate').fadeIn(1000);
			return false;
		}
	});

	$(".btnSalesStats").on("click", function(e){
		e.preventDefault();
		$("a[class='salesfeed']").trigger("click");
		$('html,body').animate({
			scrollTop: $(".dashboard").offset().top - 70
		});
	});

});
