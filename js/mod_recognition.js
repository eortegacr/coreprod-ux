var multiarray = [];
var value = "";

//Check for duplicates. load each selection into an array. when clicking to advance to next step, check array. If selection is NOT in array, add it and display the choice to the user. If not return.
function logMultiSelect( selection, img, category, href ) {
	if(multiarray.indexOf(selection) == -1){
		multiarray.push(selection);
		var acSelections = "<li class='selection'><a href='" + href + "'><img src='images/" + img + "'>" + selection + "</a><a href='#' class='close'>X</a>";
		$(acSelections).prependTo( "#recog_ACSelections" );
	}

	// when multselect close is clicked, remove this instance from the array.
	$("li.selection a").click(function(){
  		selection = $(this).parent();
		text = selection.text().slice(0,-1);
		i = multiarray.indexOf(text);
		if(i != -1){
			multiarray.splice(i, 1);	
		}
		selection.remove();
	});
}

$(document).ready(function(){
	recogSomeone();
});

function recogSomeone() {

// Locate and select employees to reward
	$( ".recog_EmpLookup" ).autocomplete({
		source: companyDirectory,
		minLength: 0,
		select: function( event, ui ) {
			logMultiSelect( ui.item.value, ui.item.img, ui.item.category, ui.item.href );
			$(this).val("");
			return false;
		}
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ){
		return $( "<li>" )
		.data('ui-autocomplete-item', item)
		.append("<a><img src='images/" + item.img + "'>" + item.value + " | " + item.category + "</a>" )
		.appendTo( ul );
		
	};

	$("input[name='recog_Level']").click(function(){
		descripName=$(this).val();
		$('.recog_LevelDescription').hide();
		$(".recog_LevelDescription:has(h3:contains('"+descripName+"'))").show();
	});

//Populate Recognition PYO with Variables
	$('.nextstep').click(function(){
		recogEmps = [];
		$.each($('#recog_ACSelections .selection'), function() {
			recogEmps.push($("a:first-child", this).html());
		});
		var recogEmpNameSample = $("#recog_ACSelections > .selection:first-child > a:first-child").text();
		var recogLevelName = $("input[name='recog_Level']:checked").val();
		var recogLevelImg = $(".level_img.selected img").attr('src');
		var recogMethodName = $("input[name='recog_Delivery']:checked").val();
		var recogMethodImg = $(".delivery_img.selected img").attr('src');
		var recogDate = $("input[name=recog_Scheduler]").val();
		var recogmessage = $("#recog_message").val();
		$("input[name='recogEmpsList']").val(recogEmps);
		if($(this).parents('fieldset').next().hasClass('recog_personalize')){
			$("td.recName > h4").html(recogLevelName);
			$("td.recEmpNameSample").html(recogEmpNameSample);
			$("td.recDate").html(recogDate);
			$("td.message").html(recogmessage);
		}
		if($(this).parents('fieldset').next().hasClass('recog_postSettings')){
			$(".recog_postMessage").html(recogmessage);
		}
		if($(this).parents('fieldset').next().hasClass('recog_review')){
			$("#recognizedPeople, #recog_ReviewLevel, #recog_ReviewMethod, #recog_ReviewMessage").empty();
			$.each($(recogEmps), function(index, value) {
				$('<li>'+value+'</li>').prependTo("#recognizedPeople");
			});
			$('#recognizedPeople li img').addClass('roundPhoto');
			$("#recog_ReviewLevel").html('<img src="'+recogLevelImg+'" /><br /> <h5>'+recogLevelName+'</h5>');
			$("#recog_ReviewMethod").html('<img src="'+recogMethodImg+'" /><br /> <h5>'+recogMethodName+'</h5>');
			$("#recog_ReviewMessage").html(recogmessage);
		}
	});

// Colorpicker for customizing certificates - Change BG and TEXT colors
	$('.minicolors').minicolors({
// in order to change css on personlized templates, capture the color input values and apply them
		change: function(hex, opacity) {
			value = $(this).attr("data_value", hex);
			value1 = $("#recog_PersonalizeColorBg").attr("data_value");
			value2 = $("#recog_PersonalizeColorText").attr("data_value");
			$("table.recog_personalizePYO").css("background-color", value1);
			$("table.recog_personalizePYO td, table.recog_personalizePYO td h4").css("color", value2);
		}
	});

	$(".graphic_radio a.personalize_img").click(function(){
		imagesrc = $('img',this).attr("src").replace('_thumb','');
		$("td.bgimage img").attr("src", imagesrc);
	});

// Bring recognition type from selection page to confirm
	$("#recogs > .recog_template_radio > a > img").click(function(){
		recogType= $(this).attr('src');
		$(function (){
			var confirmRecType = "<img src='" + recogType + "'/>";
			$("#recognizedType > img").replaceWith(confirmRecType );
		});
	});

// Bring recognition method (print, email) from selection page to confirm
	$(".method > .recog_template_radio > a > img").click(function(){
		recogMethod= $(this).attr('src');
		$(function (){
			var confirmRecMethod = "<img src='" + recogMethod + "'/>";
			$("#recognizedMethod > img").replaceWith(confirmRecMethod );
		});
	});
	
// Bring recognition text from input to review page, then to confirm
	$("#delivery > .nextstep").click( function() {
		personalizeText = $("#personalize").val();
		$("#postMsg").html(personalizeText);
		$("#confirmMessage").html(personalizeText);
	});
	
// Bring image of employee from selection page to confirm
	$("#lookup > .nextstep").click( function() {
		$("#recognizedPeople > li").remove();
		recEmpImg = $("#recog_ACSelections > .selection > a:first-child > img").attr('src');
		$(function (){
			$("#recog_ACSelections > .selection > a:first-child > img").each(function() {
			$("<li><a href=''><img src='" + recEmpImg + "' /></a></li>").prependTo("#recognizedPeople");
			});
		});
	});

// Submit recognition, show confirmation of submit
	$('#recSubmit').click(function(){
		$('#confirm').removeClass('current');
		$('#successMessage').addClass('current');
		return false;
	});
};