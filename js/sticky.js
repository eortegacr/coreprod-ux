		stickFunc = function(){
			$(".stickThis").removeClass("stickThis");
			var sidebarHeight = $(".usercolumn").height();//height of the sidebar
			var mainHeight = $(".maincontent").height();//height of the main content
			var windowHeight = $(window).height();//height of the window
			var windowWidth = $(window).width();
			//console.log ("sidebarHeight = " + sidebarHeight + " | mainHeight = " + mainHeight + " | windowHeight = " + windowHeight );
			if ($(".usercolumn").height() < $(".maincontent").height()){
				$(".usercolumn").addClass("stickThis");
				//console.log ("al is sticky");
			}
			else{
				$(".maincontent").addClass("stickThis");
				//console.log ("ras is sticky");
			}
			//console.log (windowWidth);
			$(window).scroll(function(){
				var windowTop = $(window).scrollTop();//scroll position of the window in regards to the document
				var containerOffset = $('.columncontainer').offset().top;//top of containerTop in relation to its parent
				var containerOffsetBottom = $(document).height() - $('.columncontainer').height() - containerOffset;//top of containerTop in relation to its parent
				var containerTop = (containerOffset - windowTop);//scroll position of the container in regards to the document
				var stickThisOffset = $('.stickThis').offset().top;//top of stickThis in relation to its parent
				var stickThisTop = (stickThisOffset - windowTop);//scroll position of stickThis in relation to the document
				var stickThisHeight = $(".stickThis").height();//height of stickThis
				var stickThisPosition = stickThisHeight + stickThisTop;//position of stickThis at any given time in relation to the document
				var longStickyFixedTop = windowHeight - stickThisHeight;//css value for longSticky top position when sticky effect is triggered
				//console.log ("containerTop = " + containerTop + " | containerOffsetBottom = " + containerOffsetBottom + " | containerOffset = " + containerOffset + " | windowTop = " + windowTop + " | stickThisTop = " + stickThisTop + " | stickThisHeight = " + stickThisHeight + " | stickThisPosition = " + stickThisPosition +" | longStickyFixedTop" + longStickyFixedTop);
				if (windowWidth > 800 && stickThisHeight > windowHeight && containerTop <= longStickyFixedTop) {
					$(".stickThis").css({"top" : longStickyFixedTop - containerTop - containerOffsetBottom});
					//console.log ("shorterdiv is LONGER than window");
				}
				else if (windowWidth > 800 && stickThisHeight < windowHeight && stickThisPosition <= windowHeight) {
					$(".stickThis").css({ "top" : -containerTop + containerOffset});
					//console.log ("shorterdiv is SHORTER than window");
				}
				else if (windowWidth < 800 || stickThisHeight > windowHeight && containerTop > longStickyFixedTop || stickThisHeight < windowHeight && stickThisPosition > windowHeight) {
					$(".stickThis").css({"position" : "relative" , "top" : "inherit"});
					//console.log ("this is the else statement LETITGO");
				}
			});
		};
		$(window).load(function(){
			stickFunc();
			lastHeight = $('.columncontainer').height();
			//console.log(lastHeight);
			setInterval(function checkForChanges() {
				if ($('.columncontainer').height() != lastHeight){
					console.log('container height changed '+ $('.columncontainer').height());
					$(".stickThis").removeClass("stickThis");
					stickFunc();
					lastHeight = $('.columncontainer').height(); 
				}
			}, 500)
			
		});
		
		$(window).resize(function(){
			stickFunc();
		});
		
		$(".columncontainer").resize(function(){
			stickFunc();
		});
		
		
		$(window).load(function(){
			$('.landingthanksbtn').click(function(){
				if($('.landingthanks').css('display') == 'block'){
					$('.mainOverlay').slideUp(500);
					$('.mainOverlay > div').fadeOut(500);
					return false;
				}
				else{
					$('.mainOverlay').slideUp(500);
					$('.mainOverlay > div').fadeOut(500);
					$('.landingthanks').parent('.mainOverlay').slideDown(800);
					$('.landingthanks').fadeIn(1000);
					$('.nav').removeClass('active');
					return false;
				}
			});
		});
