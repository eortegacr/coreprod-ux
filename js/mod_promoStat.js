function globalStatHighchart(){
	$(function () {
		$('#globalStats').highcharts({
			title: {text: null},
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]
			},
			labels: {
				items: [{
					html: 'Approved Sales by Promotion',
					style: {
						left: '20px',
						top: '20px',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}]
			},
			yAxis: {
				title: {
					text: 'Total approved sales'
				},
				stackLabels: {
					enabled: true,
					style: {
						fontWeight: 'bold',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'grey'
					}
				}
			},
			stackLabels: {
				enabled: false,
				style: {
					fontWeight: 'bold',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				}
			},
			plotOptions: {
				column: {
					stacking: 'normal',
				}
			},
			series: [
				{
					type: 'column',
					name: 'Basic Suite',
					data: [30, 15, 30, 55, 12, 40, 80, 98, 45, 20, 10, 30]
				},
				{
					type: 'column',
					name: 'Professional Suite',
					data: [25, 2, 20, 5, 24, 64, 36, 74, 86, 88, 17, 22]
				},
				{
					type: 'column',
					name: 'Premium Suite',
					data: [9, 20, 35, 82, 29, 99, 86, 4, 75, 86, 26, 0]
				},
				{
					type: 'spline',
					name: 'Last Year Approved Comparison',
					data: [60, 100, 60, 40, 80, 50, 100, 150, 75, 175, 40, 30],
					marker: {
						lineWidth: 2,
						lineColor: Highcharts.getOptions().colors[3],
						fillColor: 'white'
					}
				},
				{
					type: 'pie',
					name: 'Entered Sales by Promotion',
					data: [
						{
							name: 'The Brain Maze Challenge',
							y: 190,
							color: Highcharts.getOptions().colors[0]
						},
						{
							name: 'Well Done Wellness Promotion',
							y: 258,
							color: Highcharts.getOptions().colors[1]
						},
						{
							name: 'Fest 300 Vacation Sweepstakes',
							y: 866,
							color: Highcharts.getOptions().colors[2]
						}
					],
					center: [80, 80],
					size: 100,
					showInLegend: false,
					dataLabels: {enabled: false}
				}
			]
		});
	});
};

$(window).load(function(){
	globalStatHighchart();

	$("input[name='stats_Promo']").click(function(){
		descripName=$(this).val();
		$(this).parents('.graphic_radio_container').hide();
			$('.stats_PromoDescription').hide();
			$(".stats_PromoDescription:has(h3:contains('"+descripName+"'))").show();
			$(".stats_PromoDescription:has(h3:contains('"+descripName+"')) .leadSellerStat").each(function() {
				$(this).highcharts().reflow();
			});
			if($(".stats_PromoDescription:has(h3:contains('"+descripName+"')) .promoProgress").length){
				$(".stats_PromoDescription:has(h3:contains('"+descripName+"')) .promoProgress").highcharts().reflow();
				$('html,body').animate({
					scrollTop: $(".stats_PromoDescription:has(h3:contains('"+descripName+"'))").offset().top - 110
				});
			}
	});

	$(".salesfeed").click(function(){
		$("#globalStats1").fadeIn(2000);
		globalStatHighchart();
	});

	$('.statPromoBack').click(function(){
		$('.stats_PromoDescription').hide();
		$('#salesfeed .graphic_radio_container').show();
		return false;
	});

	LBdata= {
		LBP1TopSeller: [
			{name: 'Basic Suite', data: [658]},
			{name: 'Professional Suite', data: [34]},
			{name: 'Premium Suite', data: [273]}
		],
		LBP1TopPeer: [
			{name: 'Basic Suite', data: [138]},
			{name: 'Professional Suite', data: [98]},
			{name: 'Premium Suite', data: [29]}
		],
		LBP1MyPosition: [
			{name: 'Basic Suite', data: [138]},
			{name: 'Professional Suite', data: [98]},
			{name: 'Premium Suite', data: [29]}
		],
		LBP2TopSeller: [
			{name: 'Basic Suite', data: [658]},
			{name: 'Professional Suite', data: [34]},
			{name: 'Premium Suite', data: [273]}
		],
		LBP2TopPeer: [
			{name: 'Basic Suite', data: [138]},
			{name: 'Professional Suite', data: [98]},
			{name: 'Premium Suite', data: [29]}
		],
		LBP2MyPosition: [
			{name: 'Basic Suite', data: [138]},
			{name: 'Professional Suite', data: [98]},
			{name: 'Premium Suite', data: [29]}
		],
		LBP3TopSeller: [
			{name: 'Basic Suite', data: [658]},
			{name: 'Professional Suite', data: [34]},
			{name: 'Premium Suite', data: [273]}
		],
		LBP3TopPeer: [
			{name: 'Basic Suite', data: [138]},
			{name: 'Professional Suite', data: [98]},
			{name: 'Premium Suite', data: [29]}
		],
		LBP3MyPosition: [
			{name: 'Basic Suite', data: [138]},
			{name: 'Professional Suite', data: [98]},
			{name: 'Premium Suite', data: [29]}
		],
	}

	LBDefaults={
		chart: {type: 'bar'},
		credits: {enabled: false},
		colors: companyColors,
		plotOptions: {series: {stacking: 'normal'}},
		title: {text: null},
		xAxis: {categories: ['Entered Sales' ]},
		yAxis: {
			min: 0,
			title: {text: null}
		},
		legend: {reversed: true},
		series: [],
		exporting: {enabled: false}
	}

	$(".leadSellerStat").each (function(index){
		var LBUsername = $(this).attr('id');
		var LBOptions = $.extend({},LBDefaults);
		LBOptions.series = LBdata[LBUsername];
		$(this).highcharts(LBOptions);
	});

	$(function () {
		$('#promoProgress1').highcharts({
			colors: companyColors,
			credits: {enabled: false},
			chart: {type: 'bar'},
			title: {text: null},
			xAxis: {
				categories: ['Progress'],
				title: {text: null}
			},
			yAxis: {
				min: 0,
				title: {text:null},
				labels: {overflow: 'justify'}
			},
			plotOptions: {
				bar: {dataLabels: {enabled: true}}
			},
			
			series: [{
				name: 'Entered Sales',
				data: [150]
			}, {
				name: 'Your Record',
				data: [177]
			}, {
				name: 'Next Tier',
				data: [200]
			}, {
				name: '1st place',
				data: [225]
			}]
		});
	});
	
	$(function () {
		$('#promoProgress2').highcharts({
			colors: companyColors,
				credits: {enabled: false},
			chart: {
				type: 'bar'
			},
			title: {
				text: null
			},
			xAxis: {
				categories: ['Progress'],
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				title: {text:null},
				labels: {overflow: 'justify'}
			},
			plotOptions: {
				bar: {dataLabels: {enabled: true}}
			},
			series: [
				{
					name: 'Entered Sales',
					data: [150]
				},
				{
					name: 'Your Record',
					data: [177]
				},
				{
					name: 'Next Tier',
					data: [200]
				},
				{
					name: '1st place',
					data: [225]
				}
			]
		});
	});

	$(function () {
		$('#promoProgress3').highcharts({
			colors: companyColors,
				credits: {enabled: false},
			chart: {
				type: 'bar'
			},
			title: {
				text: null
			},
			xAxis: {
				categories: ['Progress'],
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				title: {text:null},
				labels: {overflow: 'justify'}
			},
			plotOptions: {
				bar: {dataLabels: {enabled: true}}
			},
			series: [
				{
					name: 'Entered Sales',
					data: [150]
				},
				{
					name: 'Your Record',
					data: [177]
				},
				{
					name: 'Next Tier',
					data: [200]
				},
				{
					name: '1st place',
					data: [225]
				}
			]
		});
	});
	
	$(function () {
		$('#promoProgress4').highcharts({
			colors: companyColors,
				credits: {enabled: false},
			chart: {
				type: 'bar'
			},
			title: {
				text: null
			},
			xAxis: {
				categories: ['Progress'],
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				title: {text:null},
				labels: {overflow: 'justify'}
			},
			plotOptions: {
				bar: {dataLabels: {enabled: true}}
			},
			series: [
				{
					name: 'Entered Sales',
					data: [150]
				},
				{
					name: 'Your Record',
					data: [177]
				},
				{
					name: 'Next Tier',
					data: [200]
				},
				{
					name: '1st place',
					data: [225]
				}
			]
		});
	});

});

/*$(function () {
	$('#promoProgress3').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'bar'},
		title: {text: null},
		xAxis: {
			categories: ['Progress'],
			title: {text: null}
		},
		yAxis: {
			min: 0,
			title: {text:null},
			labels: {overflow: 'justify'}
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		series: [
			{
				name: 'Entered Sales',
				data: [150]
			},
			{
				name: 'Your Record',
				data: [177]
			},
			{
				name: 'Next Tier',
				data: [200]
			},
			{
				name: '1st place',
				data: [225]
			}
		]
	});
});


//----------------------------------WISHLIST BARS //
saleStatsTotals=["Total"];

saleStatsbars= {
	Wishlist: [
		1500
	],
	IncreaseRank: [
		300
	],
	TopTier: [
		300
	]
}

saleStatsTotalsYmax= {
	Wishlist: [
		1550
	],
	IncreaseRank: [
		315
	],
	TopTier: [
		575
	]
};

saleStatsTotalDefaults={
chart: {
			type: 'bar'
		},
		title: {
			text: null
		},
		subtitle: {
			text: null
		},
		xAxis: {
			categories: [null],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			endOnTick: false,
			max: 2000,
			title: {
				text: null,
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: null
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		credits: {
			enabled: false
		},
		series: [{
			name: null,
			data: [1500]
		}]
}

$(".totalbar").each (function(index){

	var saleStatsTotalsid = $(this).data("stattotals");
	var saleStatsTotalsOptions = $.extend({},saleStatsTotalDefaults);
	saleStatsTotalsOptions.series = [{data: saleStatsbars[saleStatsTotalsid]}];
	saleStatsTotalsOptions.yAxis = [{max: saleStatsTotalsYmax[saleStatsTotalsid]}];
	$(this).highcharts(saleStatsTotalsOptions);
	
	
});


//----------------------------------SALES COLUMN //
	columnTotals=["Columns"]
	
	columnChartBars= {
		bars1: [10,3,8,9,3,6,7,4,7,10,2,11],
		bars2: [10,3,8,9,3,6,7,4,7,10,2,11],
		bars3: [10,3,8,9,3,6,7,4,7,10,2,11]
	}
	
	columnDefaults={
		chart: {
			type: 'column'
		},
		title: {
			text: null
		},
		subtitle: {
			text: null
		},
		xAxis: {
			categories: [
				null
			]
		},
		yAxis: {
			min: 0,
			title: {
				text: null
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [{
			name: null,
			data: [10,3,8,9,3,6,7,4,7,10,2,11]
		}]
	}
	
	$(".salescolumnz").each (function(index){
		var columnID = $(this).data("columnbars");
		var columnOptions = $.extend({},columnDefaults);
		columnOptions.series = [{data: columnChartBars[columnID]}];
		$(this).highcharts(columnOptions);
	
	});

//----------------------------------YTD LINE GRAPH //


	$('.ytdsales').highcharts({
		title: {
			text: null,
			x: -20 //center
		},
		subtitle: {
			text: null,
			x: -20
		},
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
				'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: null
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}]
		},
		tooltip: {
			valueSuffix: null
		},
		series: [{
			name: null,
			data: [1, 10, 6, 4, 7, 9, 7, 4, 4.25]
		}, {
			name: null,
			data: [1, 5, 7, 7.5, 7, 7.5, 8, 9]
		}]
	});*/
