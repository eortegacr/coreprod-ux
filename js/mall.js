//sample merchant data with the following index / column relationships
	//0 = "MERCHANT_ID"
	//1 = "MERCHANT_NAME"
	//2 = "ONLINE_MERID"
	//3 = "PYO_MERID"
	//4 = "INTELISPEND_MERID"
	//5 = "GC_MERID"
	//6 = "PYO_DENOMINATIONS"
	//7 = "GC_DENOMINATIONS"
	//8 = "image"
	//9 = "category"
	//10 = "Description"
	//11 = "Preference_denom"

//sample Category data with the following index / column relationships
	//0 = "Category Name"
	//1 = "Category Image"

var categoryData = {
	"DATA":[
		["Accessories","category_accessories.png"],
		["Apparel & Shoes","category_apparelAndShoes.png"],
		["Babies and Kids","category_babiesAndKids.png"],
		["Bed & Bath","category_bedAndBath.png"],
		["Books and Music","category_booksAndMusic.png"],
		["Décor","category_decor.png"],
		["Department Stores","category_departmentStores.png"],
		["DVD's and Movies","category_dvdsAndMovies.png"],
		["Electronics","category_electronics.png"],
		["Food and Wine","category_foodAndWine.png"],
		["Gifts","category_gifts.png"],
		["Health & Beauty","category_healthAndBeauty.png"],
		["Home & Garden","category_homeAndGarden.png"],
		["Jewelry & Watches","category_jewelryAndWatches.png"],
		["Kitchen & Cooking","category_kitchenAndCooking.png"],
		["Office","category_office.png"],
		["Pets","category_pets.png"],
		["Restaurants","category_restaurants.png"],
		["Sports and Fitness","category_sportsAndFitness.png"],
		["Toys and Games","category_toysAndGames.png"],
		["Travel & Entertainment","category_travelAndEntertainment.png"]
]};

//Empty Arrays to filter through for page load and user interactions
//array for both vendors and categories
acObjects = [];
//array for vendors only
acVendors = [];
//array for categories only
acCategory = [];

$(document).ready(function () {
	//format category data
	$.each(categoryData, function(index, value) {
		categoryInfo = value;
	});
	
	//loop formatted category data and push it to CATERGORY ONLY array AND array for Categories and Vendors. In the latter, use the name in both the name and CATERGORY field so it's searchable in the autocomplete.
	//after that append the name and image to the category results list on the middle of the page and hide it
	catagoryData = $.each(categoryInfo, function(index, value) {
		categoryName = value[0];
		categoryNameInitial = categoryName[0];
		categoryImg = value[1];
		acObjects.push({value:categoryName, img:categoryImg, category:categoryName, classification:"Category" });
		acCategory.push({value:categoryName, img:categoryImg});
		$(".categorySearchResults > ul").append("<li><img src='images/vendors/"+ categoryImg + "' /> <a href='#'>" + categoryName + "</a></li>" );
		$(".categorySearchResults > ul >li").hide();
	});

	//format the merchant data
	$.each(merchantData, function(index, value) {
		vendorInfo = value;
	});

//loop the vendor data and push it to the VENDOR Only array and the array for both categories and Vendors
//after that append the merchant name and image to the View All Vendors overlay, wrapped in a link with a # and vendor id. This will trigger the generation of the detail page with the clicked vendor displayed
	vendorData = $.each(vendorInfo, function(index, value) {
		vendorId = value[0];
		vendorName = value[1];
		vendorNameInitial = vendorName[0];
		vendorOnlineID = value[2];
		vendorPYOID = value[3];
		vendorIntSpendID = value[4];
		vendorPlasticID = value[5];
		vendorPYODenom = value[6];
		vendorPlasticDenom = value[7];
		vendorImg = value[8];
		vendorCategory = value[9];
		acObjects.push({id:vendorId, value:vendorName, img:vendorImg, category:vendorCategory, classification:"Vendor"});
		acVendors.push({value:vendorName, img:vendorImg});
		$.each(vendorCategory, function(index, value) {
			$( "div:has(.allVendorCategory:contains("+value+")) > ul" ).append("<li><a href='d_mall_detail.html#"+vendorId+"'>" + vendorName + "</a></li>" );
		});
	});

//when the pagination links are clicked, clear out the current search results, and generate the message to user of what they're filtering for and...
	$("#mallFilter > div > div > label > input:radio").click(function(){
		var radioValue = $(this).attr('value');
		$( '.searchResultsContainer').slideDown('fast');	
		$( ".vendorSearchResults > ul > li").remove();
		$(".categorySearchResults > ul >li" ).slideUp('slow');
		$('.searchResultsInfo').empty();
		$('.searchResultsInfo').append("<div class='content'><h5>Search results for categories and vendors beginning with: "+radioValue+"</h5></div>");
		$('html,body').animate({
			scrollTop: $('.mallContainer').offset().top - 70
		});

//...loop through vendor data...
		vendorData = $.each(vendorInfo, function(index, value) {
			vendorId = value[0];
			vendorName = value[1];
			vendorNameInitial = vendorName[0];
			vendorOnlineID = value[2];
			vendorPYOID = value[3];
			vendorIntSpendID = value[4];
			vendorPlasticID = value[5];
			vendorPYODenom = value[6];
			vendorPlasticDenom = value[7];
			vendorImg = value[8];
			vendorCategory = value[9];

//...for any vendor name that begins with the letter clicked, display the vendor in the vendor results list, then...
			if (vendorNameInitial == radioValue ) {
				$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+vendorId+"' class='imagelink vendorSelection'><img src='images/vendors/"+ vendorImg + "' /><br />" + vendorName + "</a></li>");
				$( ".vendorSearchResults > ul > li" ).show();
			}
		});

//...loop through the category data...
		catagoryData = $.each(categoryInfo, function(index, value) {
			categoryName = value[0];
			categoryNameInitial = categoryName[0];
			categoryImg = value[1];		
			
//...for any category name that begins with the letter clicked, display the category in the category results list
			if (categoryNameInitial == radioValue ) {		
				$( ".categorySearchResults > ul > li:has(a:contains("+categoryName+"))" ).slideDown( "slow" );
			}
		});
		return false;
	});

//when clicking on a category in the Shop By menu, clear out the current search results and generate the message to user of what they're filtering for and...
	$(".categoryFilter > li > a").click(function(){
		var selectedCat = $(this).text();
		$( '.searchResultsContainer').slideDown('fast');	
		$( ".vendorSearchResults > ul > li").remove();
		$(".categorySearchResults > ul >li" ).slideUp('slow');
		$('.searchResultsInfo').empty();
		$('.searchResultsInfo').append("<div class='content'><h5>Search results for: "+selectedCat+"</h5></div>");
		$('html,body').animate({
			scrollTop: $('.mallContainer').offset().top - 70
		});

		//...loop through vendor data...
		vendorData = $.each(vendorInfo, function(index, value) {
			vendorId = value[0];
			vendorName = value[1];
			vendorNameInitial = vendorName[0];
			vendorOnlineID = value[2];
			vendorPYOID = value[3];
			vendorIntSpendID = value[4];
			vendorPlasticID = value[5];
			vendorPYODenom = value[6];
			vendorPlasticDenom = value[7];
			vendorImg = value[8];
			vendorCategory = value[9];

//...if the vendors category list has a match to the category clicked, add the vendor to the search results
			if ($.inArray( selectedCat, vendorCategory) > -1) {		
				$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+vendorId+"' class='imagelink vendorSelection'><img src='images/vendors/"+ vendorImg + "' /><br />" + vendorName + "</a></li>");
				$( ".vendorSearchResults > ul > li" ).show();
			}
		});
		return false;
	});

	//when clicking the category in the search results, do the same as above
	$(".categorySearchResults > ul > li > a").click(function(){
		var selectedCat = $(this).text();
		$( '.searchResultsContainer').slideDown('fast');	
		$( ".vendorSearchResults > ul > li").remove();
		$(".categorySearchResults > ul >li" ).slideUp('slow');
		$('.searchResultsInfo').empty();
		$('.searchResultsInfo').append("<div class='content'><h5>Search results for: "+selectedCat+"</h5></div>");
		$('html,body').animate({
			scrollTop: $('.mallContainer').offset().top - 70
		});
		vendorData = $.each(vendorInfo, function(index, value) {
			vendorId = value[0];
			vendorName = value[1];
			vendorNameInitial = vendorName[0];
			vendorOnlineID = value[2];
			vendorPYOID = value[3];
			vendorIntSpendID = value[4];
			vendorPlasticID = value[5];
			vendorPYODenom = value[6];
			vendorPlasticDenom = value[7];
			vendorImg = value[8];
			vendorCategory = value[9];
			if ($.inArray( selectedCat, vendorCategory) > -1) {
				$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+vendorId+"' class='imagelink vendorSelection'><img src='images/vendors/"+ vendorImg + "' /><br />" + vendorName + "</a></li>");
				$( ".vendorSearchResults > ul > li" ).show();
			}
		});
		return false;
	});
	
//When a method is clicked on the Shop By menu, clear the search results, then...
	$(".methodFilter > li > a").click(function(){
		var selectedMethod = $(this).text();
		$( '.searchResultsContainer').slideDown('fast');
		$( ".vendorSearchResults > ul > li").remove();
		$(".categorySearchResults > ul >li" ).slideUp('slow');
		$('.searchResultsInfo').empty();
		$('.searchResultsInfo').append("<div class='content'><h5>Search results for: "+selectedMethod+"</h5></div>");
		$('html,body').animate({
			scrollTop: $('.mallContainer').offset().top - 70
		});

//...loop through the vendor data...
		vendorData = $.each(vendorInfo, function(index, value) {
			vendorId = value[0];
			vendorName = value[1];
			vendorNameInitial = vendorName[0];
			vendorOnlineID = value[2];
			vendorPYOID = value[3];
			vendorIntSpendID = value[4];
			vendorPlasticID = value[5];
			vendorPYODenom = value[6];
			vendorPlasticDenom = value[7];
			vendorImg = value[8];
			vendorCategory = value[9];
//..check the value of the clicked link against the ids in the vendor data. If an id exists, its a match, so display the vendor in results
			if(selectedMethod == "On-Demand"){
				if (vendorPYOID !== null) {
					$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+vendorId+"' class='imagelink vendorSelection'><img src='images/vendors/"+ vendorImg + "' /><br />" + vendorName + "</a></li>");
					$( ".vendorSearchResults > ul > li" ).show();
				}
			}
			if(selectedMethod == "Single-Store Gift Card"){
				if (vendorPlasticID !== null) {
					$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+vendorId+"' class='imagelink vendorSelection'><img src='images/vendors/"+ vendorImg + "' /><br />" + vendorName + "</a></li>");
					$( ".vendorSearchResults > ul > li" ).show();
				}
			}
			if(selectedMethod == "Multi-Store Gift Card"){
				if (vendorIntSpendID !== null) {
					$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+vendorId+"' class='imagelink vendorSelection'><img src='images/vendors/"+ vendorImg + "' /><br />" + vendorName + "</a></li>");
					$( ".vendorSearchResults > ul > li" ).show();
				}
			}
			if(selectedMethod == "Instant Redemption"){
				if (vendorOnlineID !== null) {
					$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+vendorId+"' class='imagelink vendorSelection'><img src='images/vendors/"+ vendorImg + "' /><br />" + vendorName + "</a></li>");
					$( ".vendorSearchResults > ul > li" ).show();
				}
			}
		});
		return false;
	});

//When enteries on Shop By Denominations is submitted, clear an array and push the entered values into it 
	$("#shopByDenomFilter :submit").click(function(){
		var denomArray = [];
		if($('#denomMin').val() > 0){
			denomArray.push($('#denomMin').val());
		}
		if($('#denomMax').val() > 0){
			denomArray.push($('#denomMax').val());
		}
		if($('#shopByDenomFilter > input:checked')){
			$('.checkedDenom:checked').each(function() {
				var denomCheckedValues = $(this).val().split(',');
				denomArray.push(denomCheckedValues[0]);
				denomArray.push(denomCheckedValues[1]);
			});
		}
//find the lowest and highest value in the array
		denomMin = Math.min.apply(Math, denomArray);
		denomMax = Math.max.apply(Math, denomArray);
//clear out the search results
		$( '.searchResultsContainer').slideDown('fast');	
		$( ".vendorSearchResults > ul > li").remove();
		$(".categorySearchResults > ul >li" ).slideUp('slow');
		$('.searchResultsInfo').empty();
		$('html,body').animate({
			scrollTop: $('.mallContainer').offset().top - 70
		});

//display the filter message for the user
		if(denomMax == 5000){
			$('.searchResultsInfo').append("<div class='content'><h5>Search results for denominations: "+denomMin+" - and Up</h5></div>");	
		}
		else{
			$('.searchResultsInfo').append("<div class='content'><h5>Search results for denominations: "+denomMin+" - "+denomMax+"</h5></div>");
		}

//loop through vendor data
		vendorData = $.each(vendorInfo, function(index, value) {
			vendorId = value[0];
			vendorName = value[1];
			vendorNameInitial = vendorName[0];
			vendorOnlineID = value[2];
			vendorPYOID = value[3];
			vendorIntSpendID = value[4];
			vendorPlasticID = value[5];
			vendorPYODenom = value[6];
			vendorPlasticDenom = value[7];
			vendorImg = value[8];
			vendorCategory = value[9];
			vendorPrefDenom = value[11];

//check if any denominations exist for the vendor, if so...
			if ((vendorPYODenom != null) || (vendorPlasticDenom != null) || (vendorPrefDenom != null)){
//check that the lowest value in the array is less than the highest value in the denom arrays AND check the highest value in the array is higher than the lowest value in the denom array, If so, display the vendor in the search results
				if (((denomMin <= Math.max.apply(Math, vendorPYODenom)) && (denomMax >= Math.min.apply(Math, vendorPYODenom)) || (denomMin <= Math.max.apply(Math, vendorPlasticDenom)) && (denomMax >= Math.min.apply(Math, vendorPlasticDenom)) || (denomMin <= Math.max.apply(Math, vendorPrefDenom)) && (denomMax >= Math.min.apply(Math, vendorPrefDenom)))){
					$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+vendorId+"' class='imagelink vendorSelection'><img src='images/vendors/"+ vendorImg + "' /><br />" + vendorName + "</a></li>");
					$( ".vendorSearchResults > ul > li" ).show();
				}
			}
		});
		return false;
	});

	selectedVendor = window.location.hash.substring(1);
	selectedVendorPYODenomArray = [];
	selectedVendorPlasticDenomArray = [];
	selectedVendorPrefDenomArray = [];

	vendorData = $.each(vendorInfo, function(index, value) {
		vendorId = value[0];
		vendorName = value[1];
		vendorNameInitial = vendorName[0];
		vendorOnlineID = value[2];
		vendorPYOID = value[3];
		vendorIntSpendID = value[4];
		vendorPlasticID = value[5];
		vendorPYODenom = value[6];
		vendorPlasticDenom = value[7];
		vendorImg = value[8];
		vendorCategory = value[9];
		vendorDesc = value[10];
		vendorPrefDenom = value[11];

		if(vendorId == selectedVendor){
			$( ".selectedVendorName" ).append(vendorName);
			$( ".selectedVendorDesc" ).append(vendorDesc);
			$( ".selectedVendorImg" ).attr('src', "images/vendors/"+vendorImg);
			if(vendorOnlineID != null){
				$("label:has(input[value='methodOnline'])").slideDown('fast').after( "<br />" );
			}
			if(vendorPYOID != null){
				$("label:has(input[value='methodPrint'])").slideDown('fast').after( "<br />" );
				$.each(vendorPYODenom, function(index, value) {
					selectedVendorPYODenomArray.push(value);
				});
			}
			if(vendorIntSpendID != null){
				$("label:has(input[value='methodPref'])").slideDown('fast').after( "<br />" );
				$.each(vendorPrefDenom, function(index, value) {
					selectedVendorPrefDenomArray.push(value);
				});
			}
			if(vendorPlasticDenom != null){
				$("label:has(input[value='methodPlastic'])").slideDown('fast').after( "<br />" );
				$.each(vendorPlasticDenom, function(index, value) {
					selectedVendorPlasticDenomArray.push(value);
				});
			}
			$(".selectVendorMethodOption > input").click(function(){
				$('.selectedMethodAmounts').empty();
				if($(this).val() == 'methodPrint'){
					$.each(selectedVendorPYODenomArray, function(index, value) {
						$('.selectedMethodAmounts').append("<option value='"+value+"'>$"+value+"</option>");
					});
				}
				if($(this).val() == 'methodPref'){
					$.each(selectedVendorPrefDenomArray, function(index, value) {
						$('.selectedMethodAmounts').append("<option value='"+value+"'>$"+value+"</option>");
					});
				}
				if($(this).val() == 'methodPlastic'){
					$.each(selectedVendorPlasticDenomArray, function(index, value) {
						$('.selectedMethodAmounts').append("<option value='"+value+"'>$"+value+"</option>");
					});
				}
				if($(this).val() == 'methodOnline'){
					$('.denomQtyContainer').slideUp('fast');
				}
			});
		}
	});
	$('.selectVendorMethodOption[style*="display: inline-block;"] > input:first').trigger("click");
});

$(window).load(function(){
	$('.allVendorBtn').click(function(){
		if($('.vendorList').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.vendorList').parent('.mainOverlay').slideDown(800);
			$('.vendorList').fadeIn(1000);
			return false;
		}
	});

	$('.addCartBtn').click(function(){
		if($('.addCart').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.addCart').parent('.mainOverlay').slideDown(800);
			$('.addCart').fadeIn(1000);
			return false;
		}
	});

	$('#checkoutform').submit(function(){
		if(!$(this).valid()){
			return false;
		}
		else {
			if($('.addCart').css('display') == 'block'){
				$('.mainOverlay').slideUp(500);
				$('.mainOverlay > div').fadeOut(500);
				return false;
			}
			else{
				$('.mainOverlay').slideUp(500);
				$('.mainOverlay > div').fadeOut(500);
				$('.addCart').parent('.mainOverlay').slideDown(800);
				$('.addCart').fadeIn(1000);
				return false;
			}
		}
	});
	$('.addCart > .closeParent, .addCart .continueShopping, .mainOverlay:has(div.addCart)').click(function(){
		window.location = "d_mall.html";
	});
});