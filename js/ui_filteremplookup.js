	$(document).ready(function(){

	$('.filterEmpLookup').autocomplete({
		source: companyDirectory,
		minLength: 0,
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ){
		return $( "<li>" )
			.data('ui-autocomplete-item', item)
			.append("<a><img src='images/" + item.img + "'>" + item.value + " | " + item.category + "</a>" )
			.appendTo( ul );
	};

});