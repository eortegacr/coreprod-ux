$(window).load(function(){
	$('.formWizard').each(function(){
		if($('.formWizardSections' , this).length){
			console.log($(this).siblings('h2').html() + " has sections");
			$('.formWizardSections li').eq(0).children('a').addClass('fwSectionscurrent');
			$('.nextstep', this).click(function(){
				$(this).parents("form").find("input, textarea, select, button").addClass("ignore");
				$(this).parents("fieldset").find("input.required, textarea.required, select.required, button.required").removeClass("ignore");
				if(!$(this).parents("form").valid()){
					return false;
				}
				else{
					$(this).parents('fieldset').hide().addClass('prevfs');
					$(this).parents('fieldset').next().show();
					fsNumber= $(this).parents('fieldset').next().attr('class').split(' ');
					$.each(fsNumber, function(i, val){
						console.log(val);
						if(val.indexOf("Section") >= 0){
							fsNumber = val;
							console.log("this is fsnumber" + fsNumber);
							fsSection = fsNumber.replace('Section', '') - 1;
						}
						
					});
					$(this).closest('.formWizard').find('.formWizardSections li a').removeClass('fwSectionscurrent');
					$(this).closest('.formWizard').find('.formWizardSections li').each(function() {
						if( $(this).index() < fsSection ){
							$('a',this).addClass('fwSectionsprev');
						}
						if( $(this).index() == fsSection ){
							$('a',this).addClass('fwSectionscurrent');
						}
					});
				}
			});
		
			$('.prevstep', this).click(function(){
				$(this).parents('fieldset').hide();
				$(this).parents('fieldset').prev().show().removeClass('prevfs');
				
				var fsNumber= $(this).parents('fieldset').prev().attr('class');
				var fsSection = fsNumber.replace('Section', '') - 1;
				$(this).closest('.formWizard').find('.formWizardSections a').removeClass('fwSectionscurrent').removeClass('fwSectionsprev');
				$(this).closest('.formWizard').find('.formWizardSections li').each(function() {
					if( $(this).index() < fsSection ){
						$('a',this).addClass('fwSectionsprev');
					}
					if( $(this).index() == fsSection ){
						$('a',this).addClass('fwSectionscurrent');
					}
				});
			});
		
			$('.formWizardSections', this).on('click', '.fwSectionsprev', function(){
				fsjumpto = $(this).parents('li').index() + 1;
				$(this).parents('.formWizard').find('fieldset').hide();//Hiding all the fieldsets\
				$(this).parents('.formWizard').find('fieldset.Section' + fsjumpto).first().show().removeClass('prevfs');
				$(this).parents('.formWizard').find('fieldset.Section' + fsjumpto).first().nextAll().removeClass('prevfs');
				$(this).closest('.formWizard').find('.formWizardSections a').removeClass('fwSectionscurrent').removeClass('fwSectionsprev');
				$(this).addClass('fwSectionscurrent');
				var fsSection = $(this).parent('li').prevAll().size();
				$(this).closest('.formWizard').find('.formWizardSections li').each(function() {
					if( $(this).index() < fsSection ){
						$('a',this).addClass('fwSectionsprev');
					}
				});
			});
		
		}
		else if ($('.formWizardSteps' , this).length){
			console.log($(this).siblings('h2').html() + " has steps");
				$('.formWizardSteps li' , this).eq(0).children('a').addClass('fwStepscurrent');
				//console.log($('.formWizardSteps li').eq(0).children('a').html());
		//Form Wizard, General Functions. Modules may have additional function build off of this base. ie. validation, passing values to review step, etc.
			$('.nextstep', this).click(function(){
				$(this).parents("form").find("input, textarea, select, button").addClass("ignore");
				$(this).parents("fieldset").find(".required").removeClass("ignore");
				if(!$(this).parents("form").valid()){
					console.log("not valid: " + $('.error').text());
					return false;
				}
				else{
					console.log("valid");
					$(this).parents('fieldset').hide().addClass('prevfs');
					$(this).parents('fieldset').next().show();
					$(this).closest('.formWizard').find('.formWizardSteps li a').removeClass('fwStepscurrent');
					fspassed = $(this).parents('fieldset').prevAll().size();
					console.log(fspassed);
					$(this).closest('.formWizard').find('.formWizardSteps li').each(function() {
						if( $(this).index() < fspassed ){
							$('a',this).addClass('fwStepsprev');
						}
						if( $(this).index() == fspassed ){
							$('a',this).addClass('fwStepscurrent');
						}
					});
				}
			});
		
			$('.prevstep', this).click(function(){
				$(this).parents('fieldset').hide();
				$(this).parents('fieldset').prev().show().removeClass('prevfs');
				$(this).closest('.formWizard').find('.formWizardSteps a').removeClass('fwStepscurrent').removeClass('fwStepsprev');
				fspassed = $(this).parents('fieldset').prevAll('.prevfs').size();
				$(this).closest('.formWizard').find('.formWizardSteps li').each(function() {
					if( $(this).index() < fspassed ){
						$('a',this).addClass('fwStepsprev');
					}
					if( $(this).index() == fspassed ){
						$('a',this).addClass('fwStepscurrent');
					}
				});
			});
		
			$('.formWizardSteps', this).on('click', '.fwStepsprev', function(){
				fsjumpto = $(this).parents('li').index();
				$(this).parents('.formWizard').find('fieldset').hide();
				$(this).parents('.formWizard').find('fieldset').eq(fsjumpto).show().removeClass('prevfs');
				$(this).parents('.formWizard').find('fieldset').eq(fsjumpto).nextAll().removeClass('prevfs');
				$(this).closest('.formWizard').find('.formWizardSteps a').removeClass('fwStepscurrent').removeClass('fwStepsprev');
				$(this).addClass('fwStepscurrent');
				fspassed = $(this).parent('li').prevAll().size() + 1;
				console.log(fspassed);
				$(this).closest('.formWizard').find('.formWizardSteps li').each(function() {
					if( $(this).index() < fspassed ){
						$('a',this).addClass('fwStepsprev');
					}
				});
			});
		}
	});
});