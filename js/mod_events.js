$(document).ready(function () {

	// Click on yes, remove options replace 'Will you Attend?' with answer
	$(document).on('click', '.eventYes', function () {
		$(this).parent().parent().siblings('.attend').text('Yes, I will attend');
		$(this).closest('.possAnswers').hide();
		return false;
	});
	// Click on no, remove options replace 'Will you Attend?' with answer
	$(document).on('click', '.eventNo', function () {
		$(this).parent().parent().siblings('.attend').text('No, I will not attend');
		$(this).closest('.possAnswers').hide();
		return false;
	});
	// Click on maybe, remove options replace 'Will you Attend?' with answer
	$(document).on('click', '.eventMaybe', function () {
		$(this).parent().parent().siblings('.attend').text('I may attend');
		$(this).closest('.possAnswers').hide();
		return false;
	});

});