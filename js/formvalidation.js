$(window).load(function(){

//Form Validation
	$.validator.setDefaults({
		ignore: ".ignore",
		
		errorPlacement: function(error, element) {
			if($(element).parents('.graphic_radio_container').length){
				$( element ).parents(".graphic_radio_container" ).append(error);
			}
			else{
				$( element ).after( error );
			}
		},
		
		submitHandler: function() {
			//alert("submitted!");
		}
	});

	$('.valForm').each( function(){
		var form = $(this);
		form.validate();
		
		form.submit(function(){
			  $(this).find(".required").removeClass("ignore");  // validate all input
			  if(!$(this).valid()){
				 return false;  
			  }
		
		});
	});
	


});