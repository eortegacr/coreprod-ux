//Sample Emp Data
companyDirectory = [
	{
		value: "Alba Lora",
		img: "employees/alba.jpg",
		category: "User",
		href: "AlbaLora",
		dept: "Call Center"
	},
	{
		value: "Anadeli Marron",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "AnadeliMarron",
		dept: "Call Center"
	},
	{
		value: "April Gibson",
		img: "employees/april.jpg",
		category: "User",
		href: "AprilGibson",
		dept: "Project Management"
	},
	{
		value: "Ari Slavin",
		img: "employees/ari.jpg",
		category: "User",
		href: "AriSlavin",
		dept: "Sales"
	},
	{
		value: "Brian Dodds",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "BrianDodds",
		dept: "Sales"
	},
	{
		value: "Donald Burns",
		img: "employees/don.jpg",
		category: "User",
		href: "DonaldBurns",
		dept: "IT"
	},
	{
		value: "Edward Brookshire",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "EdwardBrookshire",
		dept: "Executive"
	},
	{
		value: "Eloy Ortega",
		img: "employees/eloy.jpg",
		category: "User",
		href: "EloyOrtega",
		dept: "IT"
	},
	{
		value: "Evleen Singh",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "EvleenSingh",
		dept: "Project Management"
	},
	{
		value: "Fiona Soon",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "FionaSoon",
		dept: "Finance"
	},
	{
		value: "Gabe Samaroo",
		img: "employees/gabe.jpg",
		category: "User",
		href: "GabeSamaroo",
		dept: "IT"
	},
	{
		value: "Geeta Mallick",
		img: "employees/geeta.jpg",
		category: "User",
		href: "GeetaMallick",
		dept: "Call Center"
	},
	{
		value: "James Hemmer",
		img: "employees/jimH.jpg",
		category: "User",
		href: "JamesHemmer",
		dept: "Executive"
	},
	{
		value: "Jason Seminara",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "JasonSeminara",
		dept: "IT"
	},
	{
		value: "Jerry Hsiung",
		img: "employees/jerry.jpg",
		category: "User",
		href: "JerryHsiung",
		dept: "IT"
	},
	{
		value: "Jim King",
		img: "employees/jimK.jpg",
		category: "User",
		href: "JimKing",
		dept: "Project Management"
	},
	{
		value: "Joanne Gadson",
		img: "employees/joanne.jpg",
		category: "User",
		href: "JoanneGadson",
		dept: "Human Resources"
	},
	{
		value: "Johane Jean-Charles",
		img: "employees/johane.jpg",
		category: "User",
		href: "JohaneJean-Charles",
		dept: "Project Management"
	},
	{
		value: "Jonathan Mejia",
		img: "employees/jon.jpg",
		category: "User",
		href: "JonathanMejia",
		dept: "IT"
	},
	{
		value: "Jordan Hauge",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "JordanHauge",
		dept: "IT"
	},
	{
		value: "Kimberly Cea",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "KimberlyCea",
		dept: "Call Center"
	},
	{
		value: "Kristin Wylie",
		img: "employees/kristen.jpg",
		category: "User",
		href: "KristinWylie",
		dept: "Project Management"
	},
	{
		value: "Larry Novak",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "LarryNovak",
		dept: "IT"
	},
	{
		value: "Lena Zlotnikova",
		img: "employees/lena.jpg",
		category: "User",
		href: "LenaZlotnikova",
		dept: "Finance"
	},
	{
		value: "Marc Hollander",
		img: "employees/marc.jpg",
		category: "User",
		href: "MarcHollander",
		dept: "IT"
	},
	{
		value: "Mark Wojoski",
		img: "employees/mark.jpg",
		category: "User",
		href: "MarkWojoski",
		dept: "Sales"
	},
	{
		value: "Meredith Falb",
		img: "employees/meredith.jpg",
		category: "User",
		href: "MeredithFalb",
		dept: "Sales"
	},
	{
		value: "Michael Murry",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "MichaelMurry",
		dept: "IT"
	},
	{
		value: "Michael Uduhiri",
		img: "employees/michael.jpg",
		category: "User",
		href: "MichaelUduhiri",
		dept: "Project Management"
	},
	{
		value: "Michelle Yuen",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "MichelleYuen",
		dept: "Administration"
	},
	{
		value: "Mingbo Xu",
		img: "employees/mingbo.jpg",
		category: "User",
		href: "MingboXu",
		dept: "IT"
	},
	{
		value: "Nicholas Pizzone",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "NicholasPizzone",
		dept: "Sales"
	},
	{
		value: "Nick Pino",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "NickPino",
		dept: "Sales"
	},
	{
		value: "Nick Pompa",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "NickPompa",
		dept: "IT"
	},
	{
		value: "Pu Chen",
		img: "employees/pu.jpg",
		category: "User",
		href: "PuChen",
		dept: "IT"
	},
	{
		value: "Robert Dong",
		img: "employees/robert.jpg",
		category: "User",
		href: "RobertDong",
		dept: "IT"
	},
	{
		value: "Roselyn Augustin",
		img: "employees/rosie.jpg",
		category: "User",
		href: "RoselynAugustin",
		dept: "Call Center"
	},
	{
		value: "Steve Lien",
		img: "employees/steveL.jpg",
		category: "User",
		href: "SteveLien",
		dept: "Executive"
	},
	{
		value: "Steven Anderson",
		img: "employees/stevenA.jpg",
		category: "User",
		href: "StevenAnderson",
		dept: "Project Management"
	},
	{
		value: "Tom Silk",
		img: "employees/tom.jpg",
		category: "User",
		href: "TomSilk",
		dept: "Executive"
	},
	{
		value: "Wanda Rivera",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "WandaRivera",
		dept: "Call Center"
	},
	{
		value: "Xiaoyi Chen",
		img: "employees/xiaoyi.jpg",
		category: "User",
		href: "XiaoyiChen",
		dept: "IT"
	},
	{
		value: "Yogesh Phadke",
		img: "employees/profileDefault.jpg",
		category: "User",
		href: "YogeshPhadke",
		dept: "IT"
	},
	{
		value: "Administration",
		img: "employees/deptDefault.jpg",
		category: "Department",
		href: "DeptAdministration"
	},
	{
		value: "Call Center",
		img: "employees/deptDefault.jpg",
		category: "Department",
		href: "DeptCallCenter"
	},
	{
		value: "Executive",
		img: "employees/deptDefault.jpg",
		category: "Department",
		href: "DeptExecutive"
	},
	{
		value: "Finance",
		img: "employees/deptDefault.jpg",
		category: "Department",
		href: "DeptFinance"
	},
	{
		value: "IT",
		img: "employees/deptDefault.jpg",
		category: "Department",
		href: "DeptIT"
	},
	{
		value: "Project Mangement",
		img: "employees/deptDefault.jpg",
		category: "Department",
		href: "DeptProjectManagement"
	},
	{
		value: "Sales",
		img: "employees/deptDefault.jpg",
		category: "Department",
		href: "DeptSales"
	},
	{
		value: "Fun Committee",
		img: "employees/groupDefault.jpg",
		category: "Group",
		href: "GroupFunCommittee"
	},
	{
		value: "Safety Commitee",
		img: "employees/groupDefault.jpg",
		category: "Group",
		href: "GroupSafetyCommitee"
	},
];

//sample merchant data
var merchantData = {
"COLUMNS":[
"MERCHANT_ID","MERCHANT_NAME","ONLINE_MERID","PYO_MERID","INTELISPEND_MERID","GC_MERID","PYO_DENOMINATIONS","GC_DENOMINATIONS","image","category","Description", "Preference_denom"],

"DATA":[
	[100,"Eddie Bauer",null,null,244,null,null,null,"vendorimages_EddieBauer.png", ["Sports and Fitness","Bed & Bath","Apparel & Shoes","Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[102,"eFendos.com","M0566",null,null,null,null,null,"vendorimages_Efendos.png", ["Travel & Entertainment"]],
	[103,"Elder Beerman",null,null,244,null,null,null,"vendorimages_ElderBeerman.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Babies and Kids","Health & Beauty","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[105,"EXPRESS\/Express Men",null,null,244,null,null,null,"vendorimages_Express.png", ["Apparel & Shoes"],"With more than 600 stores across America, EXPRESS is the must-have sexy, sophisticated fashion brand for work, the weekend, or going out. It's what's new and what's now for young fashion-oriented women and men. EXPRESS is modern fashion for a fast life. Catering to a hip, 24/7 lifestyle, EXPRESS exudes style, energy and confidence. EXPRESS speaks from the point of view of a fashion insider: in-the-know yet accessible, an expert in all matters of fashion and full of endless possibility.",[20,25,50,75,100,250,500]],
	[106,"Fairmont Hotels & Resorts ",null,null,244,null,null,null,"vendorimages_FairmontHotels&Resorts.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[107,"Fairway Rewards",null,null,244,null,null,null,"vendorimages_FairwayRewards.png", ["Sports and Fitness"]," ",[20,25,50,75,100,250,500]],
	[108,"Fandango",null,249,null,null,[10.00,25.00],null,"vendorimages_Fandango.png", ["DVD's and Movies"]],
	[109,"Fantasy Toyland","P1082",null,null,null,null,null,"vendorimages_FantasyToyLand.png", ["Toys and Games"]],
	[11,"American Eagle Outfitters",null,null,244,null,null,null,"vendorimages_AmericanEagleOutfitters.png", ["Apparel & Shoes"],"Give your friends awesome #AEOSTYLE with the American Eagle Outfitters Gift Card. American Eagle Outfitters is a brand with expertly crafted, high quality jeans at our core. We create clothes that fit our customers' lives, all made to take and make your own. We're real. We fit everyone. We ARE American style. With more than 900 stores in the U.S. and Canada, and Mexico and at ae.com, there is sure to be an American Eagle Outfitters just around the corner from you. Shop On. LIVE YOUR LIFE.",[20,25,50,75,100,250,500]],
	[112,"Fleming's Prime Steakhouse ",null,null,244,null,null,null,"vendorimages_FlemingsPrimeSteakhouse.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[113,"Foot Locker",null,279,null,14,[10.00,25,50,100.00],[10.00,25,50.00],"vendorimages_FootLocker.png", ["Sports and Fitness","Apparel & Shoes"]],
	[115,"Frontgate",null,null,244,null,null,null,"vendorimages_Frontgate.png", ["Pets","Bed & Bath","Décor","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[116,"Furniture Rewards",null,null,244,null,null,null,"vendorimages_FurnitureRewards.png", ["Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[117,"Future Memories","P1080",null,null,null,null,null,"vendorimages_FutureMemories.png", ["Gifts"]],
	[118,"Gadget Bargains","P1081",null,null,null,null,null,"vendorimages_GadgetBargains.png", ["Electronics","Office","Kitchen & Cooking"]],
	[12,"Applebee's",null,276,null,100,[10.00,25.00],[10.00,25,50.00],"vendorimages_AppleBees.png", ["Restaurants"]],
	[120,"GameStop",null,null,244,null,null,null,"vendorimages_Gamestop.png", ["Toys and Games"]," ",[20,25,50,75,100,250,500]],
	[121,"Gap\/Old Navy\/Banana Republic",null,271,244,38,[10.00,25,50.00],[25.00,50.00],"vendorimages_Gap.png", ["Apparel & Shoes"],"The Options GiftCard not only lets you choose your favorite gift, but you can also choose from any of our brands - Gap, Banana Republic, Old Navy, and Piperlime.com."],
	[123,"Gift Warehouse","M0417",null,null,null,null,null,"vendorimages_GiftWarehouse.png", ["Sports and Fitness","Pets","Jewelry & Watches","Toys and Games","Accessories","Electronics","Kitchen & Cooking","Décor","Babies and Kids","Gifts","Health & Beauty","Home & Garden","Food and Wine"]],
	[124,"Golfsmith",null,null,244,null,null,null,"vendorimages_Golfsmith.png", ["Sports and Fitness","Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[126,"Guitar Center",null,null,244,null,null,null,"vendorimages_GuitarCenter.png", ["Books and Music"]," ",[20,25,50,75,100,250,500]],
	[128,"Hard Rock Cafe ",null,null,244,null,null,null,"vendorimages_HardRockCafe.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[129,"Harry and David",null,null,244,null,null,null,"vendorimages_HarryandDavid.png", ["Gifts","Food and Wine"]],
	[13,"Aquarium Restaurants ",null,null,244,null,null,null,"vendorimages_AquariumRestaurants.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[130,"Heavenly Treasures Fine Jewelry","M1170",null,null,null,null,null,"vendorimages_HeavenlyTreasures.png", ["Jewelry & Watches"]],
	[131,"Herberger's",null,null,244,null,null,null,"vendorimages_Herbergers.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Babies and Kids","Health & Beauty","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[136,"Housewares n' Things","M1273",null,null,null,null,null,"vendorimages_HousewaresnThings.png", ["Kitchen & Cooking","Home & Garden"]],
	[139,"Johnston & Murphy",null,null,244,null,null,null,"vendorimages_jackinthebox.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[14,"AreYouGame.com","P1031",null,null,null,null,null,"vendorimages_AreYouGame.png", ["Toys and Games"],"The largest game and puzzle store on the planet."],
	[140,"Jos. A. Bank Clothiers",null,null,244,52,null,[50.00],"vendorimages_JosABankClothiers.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[141,"Journeys Kidz\/Journeys",null,null,244,null,null,null,"vendorimages_Journeys.png", ["Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[142,"Journeys\/Journeys Kidz",null,null,244,null,null,null,"vendorimages_JourneysKidz.png", ["Apparel & Shoes","Accessories"]," ",[20,25,50,75,100,250,500]],
	[143,"Kmart",null,248,244,241,[25.00,100.00],[10.00,25,50,100,250.00],"vendorimages_Kmart.png", ["Sports and Fitness","Pets","Bed & Bath","Books and Music","Jewelry & Watches","Apparel & Shoes","Toys and Games","Electronics","Office","Kitchen & Cooking","Babies and Kids","DVD's and Movies","Health & Beauty","Department Stores","Home & Garden"],"Shop Kmart for brand name consumer electronics & appliances, clothing, and a whole lot more. ",[20,25,50,75,100,250,500]],
	[144,"Kohl's",null,null,null,227,null,[10.00,25,50,100.00],"vendorimages_Kohls.png", ["Sports and Fitness","Bed & Bath","Jewelry & Watches","Apparel & Shoes","Toys and Games","Accessories","Electronics","Kitchen & Cooking","Décor","Babies and Kids","Department Stores","Home & Garden"]],
	[149,"Lane Bryant",null,null,244,null,null,null,"vendorimages_LaneBryant.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[151,"Legal Sea Foods Restaurants ",null,null,244,null,null,null,"vendorimages_LegalSeaFoods.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[152,"Legal Seafoods",null,null,244,null,null,null,"vendorimages_LegalSeaFoods.png", ["Food and Wine"]," ",[20,25,50,75,100,250,500]],
	[154,"Lids",null,null,244,null,null,null,"vendorimages_Lids.png", ["Sports and Fitness","Apparel & Shoes","Accessories"]," ",[20,25,50,75,100,250,500]],
	[156,"Logan's Roadhouse",null,null,null,238,null,[10.00,25,50.00],"vendorimages_logans.png", ["Restaurants"]],
	[158,"Longhorn Steakhouse",null,null,null,228,null,[10.00,25,50.00],"vendorimages_LonghornSteakhouse.png", ["Restaurants"]],
	[159,"Lord & Taylor",null,null,244,null,null,null,"vendorimages_Lord&Taylor.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Kitchen & Cooking","Décor","Babies and Kids","Health & Beauty","Department Stores","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[160,"Lowe's Home Improvement",null,null,244,null,null,null,"vendorimages_Lowes.png", ["Kitchen & Cooking","Home & Garden"]],
	[163,"Macy's",null,253,null,53,[10.00,25,50,500.00],[10.00,25,50,100,250.00],"vendorimages_Macys.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Electronics","Kitchen & Cooking","Department Stores","Home & Garden"],"Shop at America's favorite department store or even online at macys.com for everything from fragrances to furniture and everything in between."],
	[164,"Maggiano's Little Italy",null,null,null,229,null,[10.00,25,50.00],"vendorimages_MaggianosLittleItaly.png", ["Restaurants"]],
	[167,"MasterCuts",null,null,244,null,null,null,"vendorimages_Mastercuts.png", ["Health & Beauty"]," ",[20,25,50,75,100,250,500]],
	[168,"McCormick & Schmick's ",null,null,244,null,null,null,"vendorimages_McCormick&Schmicks.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[169,"Mimi's Café",null,null,null,230,null,[50.00],"vendorimages_mimis.png", ["Restaurants"]],
	[17,"Athleta",null,null,244,null,null,null,"vendorimages_Athleta.png", ["Sports and Fitness","Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[170,"Miraval Resort & Spa ",null,null,244,null,null,null,"vendorimages_MiravalResort&Spa.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[171,"Morrco Pet Supply","m1126",null,null,null,null,null,"vendorimages_MorrcoPetSupply.png", ["Pets"]],
	[172,"Morton's Steakhouse ",null,null,244,null,null,null,"vendorimages_MortonsSteakhouse.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[173,"Motherhood Maternity",null,null,244,null,null,null,"vendorimages_MotherhoodMaternity.png", ["Apparel & Shoes","Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[174,"Nature's Emporium","M0104",null,null,null,null,null,"vendorimages_NaturesEmporium.png", ["Gifts"]],
	[175,"Neiman Marcus",null,null,244,25,null,[50.00],"vendorimages_NeimanMarcus.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Kitchen & Cooking","Décor","Babies and Kids","Health & Beauty","Department Stores","Home & Garden","Food and Wine"]," ",[20,25,50,75,100,250,500]],
	[177,"Nike ID",null,250,null,null,[25.00,100.00],null,"vendorimages_nike_pyo.png", ["Sports and Fitness","Apparel & Shoes"]],
	[178,"Nine West",null,null,244,null,null,null,"vendorimages_NineWest.png", ["Apparel & Shoes","Accessories"]," ",[20,25,50,75,100,250,500]],
	[179,"NourishingFoods.com","M0964",null,null,null,null,null,"vendorimages_NourishingFoods.png", ["Food and Wine"]],
	[18,"Babin's Seafood House ",null,null,244,null,null,null,"vendorimages_BabinsSeafoodHouse.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[181,"Oceanaire ",null,null,244,null,null,null,"vendorimages_Oceanaire.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[182,"O'Charley's  ",null,null,null,239,null,[10.00,25,50.00],"vendorimages_ocharleys.png", ["Restaurants"]],
	[183,"Old Navy",null,null,244,null,null,null,"vendorimages_OldNavy.png", ["Apparel & Shoes","Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[184,"Olive Garden",null,null,null,27,null,[10.00,25,50.00],"vendorimages_OliveGarden.png", ["Restaurants"]],
	[185,"Omaha Steaks",null,null,244,null,null,null,"vendorimages_OmahaSteaks.png", ["Food and Wine"]," ",[20,25,50,75,100,250,500]],
	[186,"Omni Hotels and Resorts ",null,null,244,null,null,null,"vendorimages_OmniHotels&Resorts.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[187,"On the Border",null,null,null,231,null,[10.00,25,50.00],"vendorimages_OnTheBorder.png", ["Restaurants"]],
	[19,"Baby GapGAP",null,271,244,38,[10.00,25,50.00],[25.00,50.00],"vendorimages_BabyGap.png",["Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[191,"Outback Steakhouse® ",null,null,244,null,null,null,"vendorimages_OutbackSteakhouse.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[193,"Overstock.com",null,257,null,null,[25.00,50.00],null,"vendorimages_overStockPyo.png", ["Pets","Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Electronics","Kitchen & Cooking","Décor","Babies and Kids","Health & Beauty","Home & Garden"]],
	[194,"Panera Bread® ",null,null,244,null,null,null,"vendorimages_PaneraBread.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[195,"Papa John's",null,null,null,240,null,[10.00,25,50.00],"vendorimages_papajohns.png", ["Restaurants"]],
	[196,"PB Teen\/Pottery Barn Kids\/Pottery Barn",null,null,244,31,null,[25.00,50,100.00],"vendorimages_PBTeen.png", ["Bed & Bath","Décor","Babies and Kids","Home & Garden"]],
	[198,"PerfectWedding.com","P1086",null,null,null,null,null,"vendorimages_PerfectWedding.png", ["Gifts"]],
	[2,"1-800 Flowers.com",null,null,244,null,null,null,"vendorimages_1800flowers.png", ["Gifts"]," ",[20,25,50,75,100,250,500]],
	[20,"Baby Gift USA","P1084",null,null,null,null,null,"vendorimages_BabyGiftUSA.png", ["Babies and Kids"],"Adorable baby gifts and affordable baby gear for your baby!"],
	[200,"Piperlime",null,null,244,null,null,null,"vendorimages_PiperLime.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[201,"Piperlime\/GAP",null,271,244,38,[10.00,25,50.00],[25.00,50.00],"vendorimages_PiperLime.png", ["Accessories","Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[202,"Pleasant Holidays® ",null,null,244,null,null,null,"vendorimages_PleasantHolidays.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[203,"Pretty Personal Gifts","M1099",null,null,null,null,null,"vendorimages_PrettyPersonalGifts.png", ["Jewelry & Watches","Babies and Kids","Gifts"]],
	[205,"Rainforest Cafe ",null,null,244,null,null,null,"vendorimages_RainforestCafe.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[206,"Red Lobster",null,null,null,32,null,[10.00,25,50.00],"vendorimages_redlobster.png", ["Restaurants"]],
	[207,"Regal Entertainment Group",null,null,244,null,null,null,"vendorimages_RegalEntertainmentGroup.png", ["Travel & Entertainment","DVD's and Movies"]," ",[20,25,50,75,100,250,500]],
	[208,"Regis Salons & Regis Signature Salons",null,null,244,null,null,null,"vendorimages_RegisSignatureSalon.png", ["Health & Beauty"]," ",[20,25,50,75,100,250,500]],
	[21,"BabyGiftIdea.com","M0583",null,null,null,null,null,"vendorimages_BabyGiftIdea.com.png", ["Babies and Kids"],"Award-winning baby store specializing in unique and personalized shower gifts! Embroidered blankets, nursery decorum, clothing, educational toys, sentimental keepsakes, religious gifts, silver & pewter, and much more! Secure online and offline ordering!"],
	[210,"Romano's Macaroni Grill",null,null,null,107,null,[10.00,25,50.00],"vendorimages_RestorationHardware.png", ["Restaurants"]],
	[212,"Roy's ",null,null,244,null,null,null,"vendorimages_Roys.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[213,"Saks Fifth Avenue",null,null,null,122,null,[50.00],"vendorimages_SaksFifthAvenue.png", ["Apparel & Shoes","Accessories","Department Stores"]],
	[214,"Sally Beauty",null,null,244,null,null,null,"vendorimages_SallyBeauty.png", ["Health & Beauty"]," ",[20,25,50,75,100,250,500]],
	[215,"Saltgrass Steakhouse ",null,null,244,null,null,null,"vendorimages_SaltgrassSteakhouse.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[216,"Samsung Awards","M1278",null,null,null,null,null,"vendorimages_SamsungAwards.png", ["Electronics"]],
	[218,"Sandals ",null,null,244,null,null,null,"vendorimages_Sandals.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[22,"Bahama Breeze Island Grill",null,null,null,221,null,[10.00,25,50.00],"vendorimages_BahamaBreeze.png", ["Restaurants"]],
	[220,"Sears",null,245,244,137,[25.00,100.00],[10.00,25,50,100,250.00],"vendorimages_Sears.png", ["Bed & Bath","Books and Music","Jewelry & Watches","Apparel & Shoes","Toys and Games","Accessories","Electronics","Office","Kitchen & Cooking","Babies and Kids","DVD's and Movies","Health & Beauty","Department Stores","Home & Garden"],"The Sears Gift Card is the perfect choice to get the Good Life. Unbeatable selection of favorite brands in appliances, computers, electronics, fitness equipment, lawn & garden, hardware, home fashions, jewelry, apparel and more. Always the right gift or award...never the wrong size, color or style.",[20,25,50,75,100,250,500]],
	[222,"Sephora",null,null,244,null,null,null,"vendorimages_Sephora.png", ["Health & Beauty"]," ",[20,25,50,75,100,250,500]],
	[224,"Shi by Journeys",null,null,244,null,null,null,"vendorimages_ShiByJourneys.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[225,"Shutterfly",null,null,244,null,null,null,"vendorimages_Shutterfly.png", ["Gifts"]," ",[20,25,50,75,100,250,500]],
	[227,"Silver Insanity","m1258",null,null,null,null,null,"vendorimages_SilverInsanity.png", ["Jewelry & Watches"]],
	[228,"SilverOnly.com","M0634",null,null,null,null,null,"vendorimages_SilverOnly.png", ["Jewelry & Watches"]],
	[229,"SonyAwards.com","m1182",null,null,null,null,null,"vendorimages_SonyAwards.png", ["Electronics"]],
	[230,"SpaFinder",null,null,244,98,null,[50.00],"vendorimages_SpaFinderWellness.png", ["Health & Beauty"]," ",[20,25,50,75,100,250,500]],
	[233,"Sports Authority","M1161",null,null,null,null,null,"vendorimages_SportsAuthority.png", ["Sports and Fitness","Apparel & Shoes"]],
	[234,"Sports Gifts","p1083",null,null,null,null,null,"vendorimages_SportsGifts.png", ["Sports and Fitness"]],
	[235,"SportsFanfare.com","P1005",null,null,null,null,null,"vendorimages_SportsFanfare.png", ["Sports and Fitness"]],
	[236,"Staples",null,null,244,null,null,null,"vendorimages_Staples.png", ["Office"]," ",[20,25,50,75,100,250,500]],
	[238,"Sunglass Hut",null,null,244,null,null,null,"vendorimages_SunglassHut.png", ["Accessories"]," ",[20,25,50,75,100,250,500]],
	[239,"SuperClubs® Resorts ",null,null,244,null,null,null,"vendorimages_SuperClubsResorts.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[24,"Banana Republic",null,271,244,152,[10.00,25,50.00],[25.00,50,100.00],"vendorimages_BananaRepublic.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[240,"Talbots",null,null,244,null,null,null,"vendorimages_Talbots.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[241,"TGI Fridays",null,null,null,37,null,[10.00,25,50,100.00],"vendorimages_TGIF.png", ["Restaurants"]],
	[243,"The Children's Place",null,null,244,null,null,null,"vendorimages_TheChildrensPlace.png", ["Apparel & Shoes","Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[245,"The Container Store",null,null,244,null,null,null,"vendorimages_theContainerStore.png", ["Bed & Bath","Office","Kitchen & Cooking","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[246,"The Crab House ",null,null,244,null,null,null,"vendorimages_TheCrabHouse.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[247,"The Equinox Resort & Spa ",null,null,244,null,null,null,"vendorimages_TheEquinoxResort&Spa.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[249,"The Limited",null,null,244,null,null,null,"vendorimages_TheLimited.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[25,"Barewalls.com","M1098",null,null,null,null,null,"vendorimages_BareWalls.png", ["Décor"]],
	[250,"The Perfume Spot","M0252",null,null,null,null,null,"vendorimages_ThePerfumeSpot.png", ["Health & Beauty"]],
	[251,"Ticketjones.com",null,null,244,null,null,null,"vendorimages_TicketJones.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[255,"TotsRoom.com","P1085",null,null,null,null,null,"vendorimages_TotsRoom.png", ["Babies and Kids"]],
	[256,"Tourneau Watches",null,null,244,null,null,null,"vendorimages_TourneauWatches.png", ["Jewelry & Watches"]," ",[20,25,50,75,100,250,500]],
	[263,"Uno",null,null,null,243,null,[10.00,25,50.00],"vendorimages_Unos.png", ["Restaurants"]],
	[265,"Vera Bradley",null,null,244,null,null,null,"vendorimages_VeraBradley.png", ["Accessories","Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[266,"Vic & Anthony's ",null,null,244,null,null,null,"vendorimages_Vic&Anthonys.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[269,"west elm",null,null,244,null,null,null,"vendorimages_WestElm.png", ["Bed & Bath","Office","Kitchen & Cooking","Décor","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[27,"Barnes & Noble",null,260,null,153,[10.00,25.00],[5.00,10,25,50,100.00],"vendorimages_BarnesAndNoble.png", ["Books and Music"],"Barnes & Noble eGift Cards let you choose from an unmatched selection of books, CDs & DVDS, toys & games, and more. You can also redeem them for NOOK Books™ & NOOK Readers. They can be used online at BN.com (www.bn.com) or at a Barnes & Noble store.<br><br><b>Terms & Conditions </b><br><br>Barnes & Noble eGift Cards can be redeemed at any Barnes & Noble store and online at BN.com (www.bn.com). They can also be used at any Barnes & Noble College and Bookstar location. Barnes & Noble eGift Cards may be used to purchase annual memberships in the Barnes & Noble Membership program (continuous billing memberships requires a valid credit card). Dormancy fees do not apply to balances on Barnes & Noble eGift Cards. The Barnes & Noble eGift Card will not be exchangeable for cash, except where required by law. BN.com will not be responsible for lost or stolen Barnes & Noble eGift Cards.<br><br>Barnes & Noble is not a sponsor or co-sponsor of this promotion. Please see back of gift card for terms and conditions of use. Barnes & Noble is not liable for any alleged or actual claims related to this offer."],
	[271,"Williams-Sonoma",null,null,244,null,null,null,"vendorimages_WilliamSonoma.png", ["Kitchen & Cooking","Food and Wine"]," ",[20,25,50,75,100,250,500]],
	[272,"Willie G's Seafood and Steakhouse ",null,null,244,null,null,null,"vendorimages_WillieGsSeafood&Steakhouse.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[274,"Wynn Las Vegas ",null,null,244,null,null,null,"vendorimages_WynnLasVegas.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[275,"Yager's Flies","M0355",null,null,null,null,null,"vendorimages_YagersFlies.png", ["Sports and Fitness"]],
	[276,"Younkers",null,null,244,null,null,null,"vendorimages_Younkers.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Babies and Kids","Health & Beauty","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[277,"Zappos.com",null,null,244,null,null,null,"vendorimages_Zappos.png", ["Bed & Bath","Apparel & Shoes","Accessories","Kitchen & Cooking","Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[279,"Starbucks",null,null,null,99,null,[5.00,10,25,50.00],"vendorimages_StarBucks.png", ["Restaurants"]],
	[281,"Target",null,null,null,134,null,[25.00,50,100.00],"vendorimages_Target.png", ["Department Stores"]],
	[282,"Amazon",null,null,null,265,null,[25.00,50,100.00],"vendorimages_Amazon.png", ["Accessories","Apparel & Shoes","Babies and Kids","Bed & Bath","Books and Music","Décor","Department Stores","DVD's and Movies","Electronics","Food and Wine","Gifts","Health & Beauty","Home & Garden","Jewelry & Watches","Kitchen & Cooking","Office","Pets","Restaurants","Sports and Fitness","Toys and Games","Travel & Entertainment"]],
	[286,"Ferrell Gas",null,null,null,209,null,[25.00],"vendorimages_ferrellgas.png", ["Travel & Entertainment"]],
	[29,"Bass Pro Shops",null,null,244,222,null,[50.00],"vendorimages_BassPro.png", ["Sports and Fitness","Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[3,"1-800 Pet Supplies",null,null,244,null,null,null,"vendorimages_1800PetSupplies.png", ["Pets"]," ",[20,25,50,75,100,250,500]],
	[31,"Bath and Body Works",null,null,244,154,null,[10.00,25,50.00],"vendorimages_BathAndBodyWorks.png", ["Health & Beauty"]," ",[20,25,50,75,100,250,500]],
	[32,"Beaches ",null,null,244,null,null,null,"vendorimages_Beaches.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[35,"Bed Bath & Beyond",null,null,244,null,null,null,"vendorimages_bedbathbeyond.png", ["Bed & Bath","Kitchen & Cooking","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[36,"Belk",null,null,null,235,null,[10.00,25,50,100.00],"vendorimages_Belk.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Kitchen & Cooking","Décor","Babies and Kids","Health & Beauty","Department Stores"]],
	[37,"Bergdorf Goodman",null,null,244,null,null,null,"vendorimages_BergdorfGoodman.png", ["Jewelry & Watches","Apparel & Shoes","Accessories","Babies and Kids","Health & Beauty","Department Stores","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[38,"Bergner's",null,null,244,null,null,null,"vendorimages_Bergners.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Babies and Kids","Health & Beauty"]," ",[20,25,50,75,100,250,500]],
	[39,"Big Fish Seafood Bistro ",null,null,244,null,null,null,"vendorimages_BigFishSeafoodBistro.png", ["Restaurants"]],
	[40,"BlanketsnMore.com","m1122",null,null,null,null,null,"vendorimages_BlanketsNMore.png", ["Bed & Bath"]," We carry the finest products from Biederlack - pillows, blankets and throws."],
	[43,"Bloomingdales",null,null,null,157,null,[50.00],"vendorimages_Bloomingdales.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Kitchen & Cooking","Babies and Kids","Health & Beauty","Department Stores","Home & Garden"]],
	[44,"Body Basics Fitness Equipment",null,null,244,null,null,null,"vendorimages_BodyBasicsFitnessEquipment.png", ["Sports and Fitness"]," ",[20,25,50,75,100,250,500]],
	[45,"Bone & Jade Art Place","M0852",null,null,null,null,null,"vendorimages_BoneandJadeArtPlace.png", ["Jewelry & Watches"],"The Bone Art Place is dedicated to the art of hand crafted New Zealand Maori bone carving and jade carving in its many forms, from the very traditional pieces to the more contemporary designs."],
	[46,"Bonefish Grill ",null,null,244,null,null,null,"vendorimages_BonefishGrill.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[47,"Bonsai Boy of New York","M0381",null,null,null,null,null,"vendorimages_BonsaiBoyofNewYork.png", ["Home & Garden"],"Bonsai Boy of New York is a family operated, certified New York State nursery. As tree growers, we maintain superior product quality and offer our trees and other accessories at low wholesale prices."],
	[48,"Bon-Ton",null,null,244,null,null,null,"vendorimages_Bos-Ton.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Health & Beauty","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[49,"Boston Market",null,null,null,242,null,[50.00],"vendorimages_BostonMarket.png", ["Restaurants"]],
	[5,"Absolute Socks","M0998",null,null,null,null,null,"vendorimages_AbsoluteSocks.png", ["Accessories"]],
	[50,"Boston Store",null,null,244,null,null,null,"vendorimages_BostonStore.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Babies and Kids","Health & Beauty","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[51,"BrandsRetail.com","M1070",null,null,null,null,null,"vendorimages_BrandsRetail.png", ["Kitchen & Cooking","Babies and Kids","Health & Beauty","Home & Garden"],"BrandsRetail.com Americas favorite online store selling general merchandise to the public. We offer FREE SHIPPING ALL YEAR AROUND."],
	[52,"Brenner's Steakhouse ",null,null,244,null,null,null,"vendorimages_BrennersSteakhouse.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[53,"Brooks Brothers",null,null,244,null,null,null,"vendorimages_BrooksBrothers.png", ["Bed & Bath","Apparel & Shoes","Accessories","Décor","Babies and Kids"]," ",[20,25,50,75,100,250,500]],
	[54,"Brookstone",null,null,244,null,null,null,"vendorimages_brookstone.png", ["Pets","Bed & Bath","Toys and Games","Electronics","Office","Kitchen & Cooking","Gifts","Health & Beauty","Home & Garden","Food and Wine"]," ",[20,25,50,75,100,250,500]],
	[55,"Brown Derby International Wine",null,null,244,null,null,null,"vendorimages_BrownDerbyInternationalWine.png", ["Food and Wine"]," ",[20,25,50,75,100,250,500]],
	[56,"Bubba Gump Shrimp Co. ",null,null,244,null,null,null,"vendorimages_BubbaGumpShrimpCo.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[57,"Build-a-Bear Workshop",null,null,244,null,null,null,"vendorimages_BuildABearWorkshop.png", ["Toys and Games"]," ",[20,25,50,75,100,250,500]],
	[59,"Cabela's",null,null,244,163,null,[50.00,100.00],"vendorimages_Cabelas.png", ["Sports and Fitness","Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[6,"Absolute Ties Online","M0322",null,null,null,null,null,"vendorimages_absoluteties.png", ["Accessories"],"Absolute Ties Online offers a full line of fun novelty neckties, suspenders, socks, and scarves. We have a special item for everyone. Games, shows, occupations, sports, teams, art, animals.... you name it and we have a tie for it! "],
	[60,"Cadillac Bar ",null,null,244,null,null,null,"vendorimages_CadillacBar.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[62,"California Pizza Kitchen ",null,null,244,null,null,null,"vendorimages_CaliforniaPizzaKitchen.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[63,"Callaway Golf",null,null,null,223,null,[50.00],"vendorimages_Callaway.png", ["Sports and Fitness","Apparel & Shoes"]],
	[64,"Cameras Direct","M1279",null,null,null,null,null,"vendorimages_CamerasDirect.png", ["Electronics"],"Cameras Direct offers you hundreds of camera and camcorder choices from all the leading brands including Sony, Canon, Nikon, Fuji and Samsung."],
	[66,"Carnival Cruise Lines® ",null,null,244,null,null,null,"vendorimages_CarnivalCruiseLines.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[67,"Carrabba's Italian Grill ",null,null,244,null,null,null,"vendorimages_CarrabbasItalianGrill.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[68,"Carson's",null,null,244,null,null,null,"vendorimages_Carsons.png", ["Bed & Bath","Jewelry & Watches","Apparel & Shoes","Accessories","Babies and Kids","Health & Beauty","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[69,"Catherines",null,null,244,null,null,null,"vendorimages_Catherines.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[7,"Adidas",null,null,244,null,null,null,"vendorimages_Adidas.png", ["Sports and Fitness","Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[70,"CB2",null,null,244,null,null,null,"vendorimages_CB2.png", ["Bed & Bath","Office","Décor","Home & Garden"]," ",[20,25,50,75,100,250,500]],
	[71,"CD Universe","P0012",null,null,null,null,null,"vendorimages_CDUniverse.png", ["Books and Music"],"Shop CD Universe for the Web's best selection, great prices, and fast shipping on compact discs, DVD and VHS movies, and video games. "],
	[72,"Charley's Crab ",null,null,244,null,null,null,"vendorimages_CharleysCrab.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[73,"Chart House Restaurant ",null,null,244,null,null,null,"vendorimages_ChartHouseRestaurant.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[74,"Cheeseburger in Paradise ",null,null,244,null,null,null,"vendorimages_CheeseburgerInParadise.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[75,"Chili's",null,277,null,160,[10.00,25.00],[10.00,25,50.00],"vendorimages_Chilis.png", ["Restaurants"]],
	[76,"Claim Jumper Restaurants ",null,null,244,null,null,null,"vendorimages_ClaimJumperRestaurants.png", ["Restaurants"]," ",[20,25,50,75,100,250,500]],
	[77,"Classic Vacations ",null,null,244,null,null,null,"vendorimages_ClassicVacations.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[78,"CollectibleJewels.com","M0641",null,null,null,null,null,"vendorimages_CollectibleJewels.png", ["Jewelry & Watches"]],
	[79,"Computer Books Online","M0555",null,null,null,null,null,"vendorimages_ComputerBooksOnline.png", ["Books and Music"]],
	[8,"Aerie",null,null,244,null,null,null,"vendorimages_Aerie.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[81,"Copper Mountain Resort ",null,null,244,null,null,null,"vendorimages_CopperMountainResort.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[82,"Cosmetic Discounter","P1045",null,null,null,null,null,"vendorimages_CosmeticDiscounter.png", ["Health & Beauty"]],
	[83,"CosmeticsCounter.com","P1068",null,null,null,null,null,"vendorimages_CosmeticsCounter.png", ["Health & Beauty"]],
	[84,"Crate & Barrel",null,null,244,165,null,[25.00,50.00],"vendorimages_Crate&Barrel.png", ["Bed & Bath","Kitchen & Cooking","Décor","Gifts","Home & Garden","Food and Wine"]," ",[20,25,50,75,100,250,500]],
	[85,"Crutchfield",null,256,null,166,[25.00,100.00],[25.00,50,100.00],"vendorimages_Crutchfield.png", ["Electronics"]],
	[86,"CUSP",null,null,244,null,null,null,"vendorimages_CUSP.png", ["Jewelry & Watches","Apparel & Shoes","Accessories"]," ",[20,25,50,75,100,250,500]],
	[89,"Dave & Buster's ",null,null,244,null,null,null,"vendorimages_Dave&Busters.png", ["Restaurants"]],
	[9,"Aeropostale",null,null,244,null,null,null,"vendorimages_Aeropostale.png", ["Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[92,"Demin Express","M1027",null,null,null,null,null,"vendorimages_DenimExpress.png", ["Apparel & Shoes"]],
	[93,"Destination Maternity",null,null,244,null,null,null,"vendorimages_DestinationMaternity.png", ["Apparel & Shoes","Babies and Kids","Health & Beauty"]," ",[20,25,50,75,100,250,500]],
	[95,"Dillard's",null,null,244,92,null,[50.00],"vendorimages_Dillards.png", ["Bed & Bath","Apparel & Shoes","Accessories","Kitchen & Cooking","Décor","Babies and Kids","Health & Beauty","Department Stores"]," ",[20,25,50,75,100,250,500]],
	[97,"Disney Gift Card ",null,null,244,null,null,null,"vendorimages_DisneyGiftCard.png", ["Travel & Entertainment"]," ",[20,25,50,75,100,250,500]],
	[98,"Easy Spirit",null,null,244,null,null,null,"vendorimages_EasySpirit.png", ["Sports and Fitness","Apparel & Shoes"]," ",[20,25,50,75,100,250,500]],
	[99,"Eat Gourmet","P1087",null,null,null,null,null,"vendorimages_EatGourmet.png", ["Kitchen & Cooking","Food and Wine"]],
]};

//highcharts color defaults
companyColors = ['#2ad','#8c4','#93b','#777','#ddd','#eee'];

$(window).load(function(){

	$('#dashboard input[type="radio"]').prop('checked', false);

/*	$("a[href*='#']").click( function() {
		var here = url.substring(url.lastIndexOf('/') + 1);
		var target = $(this).attr("href");
		if ( $(this).attr("href") == window.location.href ) {
			window.location.reload(true);
			return false;
		}
	});*/
	
	$( "form" ).on( "submit", function( event ) {
		event.preventDefault();
		$( this ).serialize();
		console.log( $( this ).serialize() );
	});
	
	
	$( "#addValueForm" ).on( "submit", function() {
		if(!$(this).valid()){
			return false;
		}
		else {
		$(this).hide();
		$('.addValueConfirm').show();
		}
	});

//style number with commas for everything with a class of "digits"
	$.fn.digits = function(){ 
		return this.each(function(){ 
			if ($(this).text().indexOf(".") !=-1){
				var parts = $(this).text().split(".");
				parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				parts[1] = parts[1].substring(0, 2);
				$(this).text(parts.join("."));
			}else{
				$(this).text( $(this).text().replace(/(\d)(?=([^.]{3})+($|[.]))/g,"$1,") );
			}
		})
	}
	$(".digits").digits();

//style number from with ordinal suffix for everything with a class of "ordinal". ex. 3 to 3rd, 13 to 13th
	$('.ordinal').each(function () {
		var lastChar = $(this).text().slice(-1);
		if (lastChar == 1 && $(this).text() != '11') {
			$('<sup>st</sup>').appendTo(this);
		}
		else if (lastChar == 2 && $(this).text() != '12') {
			$('<sup>nd</sup>').appendTo(this);
		}
		else if ((lastChar == 3 && $(this).text() != '13')) {
			$('<sup>rd</sup>').appendTo(this);
		}
		else {
			$('<sup>th</sup>').appendTo(this);
		}
	});

//limits inputs on text feilds to only Numbers for everything with a class of "numberOnly"
	jQuery.fn.forceNumeric = function () {
		return this.each(function () {
			$(this).keydown(function (e) {
				var key = e.which || e.keyCode;
				if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
				// numbers   
				key >= 48 && key <= 57 ||
				// Numeric keypad
				key >= 96 && key <= 105 ||
				// comma, period and minus, . on keypad
				key == 190 || key == 188 || key == 109 || key == 110 ||
				// Backspace and Tab and Enter
				key == 8 || key == 9 || key == 13 ||
				// Home and End
				key == 35 || key == 36 ||
				// left and right arrows
				key == 37 || key == 39 ||
				// Del and Ins
				key == 46 || key == 45)
				return true;
				return false;
			});
		});
	};
	$(".numberOnly").forceNumeric();

	$('.btnRefresh').click(function(){
		location.reload();	
	});

	$('.collapseBtn').click(function(){
		$(this).next().slideToggle('slow');
			if ( $('span', this).text() == '▶' ) {
			$('span', this).text('▼');
			}
			else {
				$('span', this).text('▶');
			}
		return false;
	});

	$(".graphic_radio a").click(function(){
		var selectedId = $(this).attr('id');
		$(this).parents('.graphic_radio_container').find('a').removeClass('selected');
		$(this).addClass('selected');
		$(this).parents('.graphic_radio_container').find("input[type='radio'][id='"+selectedId+"']").click();
	});

	$(document).on('click', '.addAnotherBtn', function(){
		var addAnother = $(this).prev('.addAnother').html();
		$(this).prev('.addAnother').after("<div class='content addAnother'>"+addAnother+"</div>");
		return false;
	});

	$('.sendMessage').click(function(event){
		if($.trim($(this).parents('form').find('textarea').val()) == ""){
			event.preventDefault();
			return false;
		}
		$('.messagePeerForm').hide();
		$('.messagePeerConfirmation').show();
	});

	$('#mobilemenu').click(function(){
		$('.nav').toggleClass('active');
		$('.mainOverlay').slideUp(500);
		return false;
	});

//OVERLAYS BELOW
	$('.closeParent').click(function(){
		$(this).parents('.mainOverlay').slideUp(500);
			$(this).parent().fadeOut(500);
		return false;
	});

	$(".mainOverlay").click(function(){
		$(this).fadeOut();
		$('.mainOverlay > div').fadeOut();
		return false;
	});

	$('.mainOverlay > div').click(function(event) {
		event.stopPropagation();
	});

	$('.bio a').click(function(){
		if($('.submenuProfile').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.submenuProfile').parent('.mainOverlay').slideDown(800);
			$('.submenuProfile').fadeIn(1000);
			$('.nav').removeClass('active');
			return false;
		}
	});

	$('.menuMall').click(function(){
		if($('.submenuMall').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.submenuMall').parent('.mainOverlay').slideDown(800);
			$('.submenuMall').fadeIn(1000);
			$('.nav').removeClass('active');
			return false;
		}
	});

	$('.menuPromo').click(function(){
		if($('.submenuPromo').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.submenuPromo').parent('.mainOverlay').slideDown(800);
			$('.submenuPromo').fadeIn(1000);
			$('.nav').removeClass('active');
			return false;
		}
	});

	$('.menuReport').click(function(){
		if($('.submenuReport').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.submenuReport').parent('.mainOverlay').slideDown(800);
			$('.submenuReport').fadeIn(1000);
			$('.nav').removeClass('active');
			return false;
		}
	});

	$('.addValueBtn').click(function(){
		if($('.addValue').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.addValue').parent('.mainOverlay').slideDown(800);
			$('.addValue').fadeIn(1000);
			return false;
		}
	});

	$('.nameReportsBtn').click(function(){
		if($('.nameReports').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.nameReports').parent('.mainOverlay').slideDown(800);
			$('.nameReports').fadeIn(1000);
			return false;
		}
	});

	$('.subsribeReportsBtn').click(function(){
		if($('.subsribeReports').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.subsribeReports').parent('.mainOverlay').slideDown(800);
			$('.subsribeReports').fadeIn(1000);
			return false;
		}
	});

	$('.productOneBtn').click(function(){
		if($('.productOne').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.productOne').parent('.mainOverlay').slideDown(800);
			$('.productOne').fadeIn(1000);
			return false;
		}
	});

	$('.productTwoBtn').click(function(){
		if($('.productTwo').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.productTwo').parent('.mainOverlay').slideDown(800);
			$('.productTwo').fadeIn(1000);
			return false;
		}
	});

	$('.productThreeBtn').click(function(){
		if($('.productThree').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.productThree').parent('.mainOverlay').slideDown(800);
			$('.productThree').fadeIn(1000);
			return false;
		}
	});
//SOCIA
//SOCIAL STREAM FUNCTIONS BELOW
	$('.peersSharedPostBtn').click(function(){
		if($('.peersSharedPost').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.peersSharedPost').parent('.mainOverlay').slideDown(800);
			$('.peersSharedPost').fadeIn(1000);
			return false;
		}
	});

	$('.peersCongratsPostBtn').click(function(){
		var overlayMessage= $(this).parent().text();
		$('.peersCongratsPost h1').empty();
		$('.peersCongratsPost h1').append(overlayMessage);
		if($('.peersCongratsPost').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.peersCongratsPost').parent('.mainOverlay').slideDown(800);
			$('.peersCongratsPost').fadeIn(1000);
			return false;
		}
	});

	$('.peersAgreePostBtn').click(function(){
		var overlayMessage= $(this).text();
		$('.peersAgreePost h1').empty();
		$('.peersAgreePost h1').append(overlayMessage);
		if($('.peersAgreePost').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.peersAgreePost').parent('.mainOverlay').slideDown(800);
			$('.peersAgreePost').fadeIn(1000);
			return false;
		}
	});

	$('.postedMediaBtn').click(function(){
		if($('.postedMedia').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.postedMedia').parent('.mainOverlay').slideDown(800);
			$('.postedMedia').fadeIn(1000);
			return false;
		}
	});

	$('.postCongratsLink').click(function(){
		peersAgreetext=$(this).parent().prev().children('.peersCongratsPostBtn').text();
		$(this).empty();
		$(this).next('.postCongratsBreak').empty();
		$(this).parent().prev().children('.peersCongratsPostBtn').empty();
		$(this).parent().prev().children('.peersCongratsPostBtn').append("You and "+peersAgreetext);
		return false;
	});

	$('.postCommentsLink').click(function(){
		$('html,body').animate({
			scrollTop: $(this).parents('.post').find('.commentForm').offset().top - 70
		});
		$(this).parents('.post').find('.commentForm textarea').focus();
	});

	$('.commentViewAllBtn').click(function(){
		$(this).parents('.post').find('.comment').slideDown('fast');
		return false;
	});

	$('.postAgreeLink').click(function(){
		peersAgreetext=$(this).siblings('.peersAgreePostBtn').text();
		console.log(peersAgreetext);
		$(this).empty();
		$(this).siblings('.postCongratsBreak').empty();
		$(this).siblings('.peersAgreePostBtn').empty();
		$(this).siblings('.peersAgreePostBtn').append("You and "+peersAgreetext);
		return false;
	});

//APPEARS IN MILESTONE MODULE AND UPCOMING MILESTONES REPORT
	$('.milestoneCongrateLink').click(function(){
		var peerName= $(this).siblings('.milestonePeer').find('.milestoneEmpName').text();
		$('.peerName').empty();
		$('.peerName').text(peerName);
		if($('.milestoneCongrate').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.milestoneCongrate').parent('.mainOverlay').slideDown(800);
			$('.milestoneCongrate').fadeIn(1000);
			return false;
		}
	});

//APPEARS IN MILESTONE MODULE AND UPCOMING MILESTONES REPORT

	$('.messagePeerBtn').click(function(){
		var peerName= $(this).siblings('h5').text();
		$('.peerName').empty();
		$('.peerName').text(peerName);
		$(this).parents('.mainOverlay').children('div').hide();
		$('.messagePeer').fadeIn('slow');
	});

	$('.milestoneCongrateAllLink').click(function(){
		if($('.milestoneCongrateAll').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.milestoneCongrateAll').parent('.mainOverlay').slideDown(800);
			$('.milestoneCongrateAll').fadeIn(1000);
			return false;
		}
	});

	$(".usersComment").keypress(function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if ((code == 13) && ($.trim($(this).val()) !== "")) {
			newcomment = $(this).val();
			$(this).parents('.post').find('.commentContainer' ).append( "<div class='comment userCommentAdded'><p class='b1o2 content'><a href='d_settings.html'><img src='images/employees/samantha.jpg' class='roundPhoto commenterPhoto' />&nbsp;Samantha Rodriguez-Gonzalez</a></p><p class='b1o2 content alignRight'><a href='#' class='peersSharedPostBtn'>0 Peers Agree</a></p><br class='clear' /><p class='commentText'>" + newcomment + "</p></div>" );
			$('.usersComment').val('');
			newcomment = null;
			return false;
		}
	});

});

$(document).ready(function() {
	$('.removeTag').click(function() {
		$(this).parents('.searchResult').remove();
	});
});
						
//GRAVEYARD OF UNSED CODE. NOT DELETED AS IT MAY COME IN HANDY LATER
/*


	$('.nav .navContent ul li a').hover(
		function(){
			var imagesrc = $("img",this).attr('src');
			$("img",this).attr('src', imagesrc.replace('.png', '_on.png'));
		},
		function(){
			var imagesrc = $("img",this).attr('src');
			$("img",this).attr('src', imagesrc.replace('_on.png', '.png'));
		}
	);

$(window).keydown(function(event){
		if(event.keyCode == 13)  {
			event.preventDefault();
			return false;
		}
	});

$('.vendorList > div > .closeParent').click(function(){
		$(this).parent().parent().parent().slideUp(500);
			$(this).parent().parent().fadeOut(500);
		return false;
	});*/

	/*$('.searchResult > form > .closeParent').click(function(){
		$(this).parent('form').parent('.searchResult').fadeOut(500);
		return false;
	});

	$('.mainOverlay > .approvalForm > .closeParent').click(function(){
		$(this).parent('.approvalForm').parent('.mainOverlay').slideUp(500);
		return false;
	});*/

	/*$('.sendCongrats').click(function(){
		$(this).parents('.milestoneMessage').hide();
		$('.milestoneMessageConfirmed').show();
	});*/
	
	/*formSize = $('.formWizard fieldset').size();*/


       /* $('.masterTooltip').hover(function(){
            // Hover over code
            var title = $(this).attr('title');
            $(this).data('tipText', title).removeAttr('title');
            $('<p class="tooltip"></p>')
            .text(title)
            .appendTo('body')
            .fadeIn('slow');
       }, function() {
            // Hover out code
           $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
       }).mousemove(function(e) {
           var mousex = e.pageX + 20; //Get X coordinates
            var mousey = e.pageY + 10; //Get Y coordinates
            $('.tooltip')
            .css({ top: mousey, left: mousex })
       });
	
        var sumofpages = $(".wrapper > div[class*='page']").size();
        wprcalc1 = 140 * sumofpages;
        var wrapperheight = sumofpages * 100;
        var pageheight = 100 / sumofpages;
	
		$(".wrapper").css( "height", "calc(" + wrapperheight + "% - " + wprcalc1 +"px)");
		$(".wrapper > div[class*='page']").css( "height", pageheight + "%");*/		

//smart scrolling
/*	
	pagepfx = "page";
	pagesfxa = 1;
	pagesfxb = 2;
	pagetotal = pagepfx + pagesfxa;
	wt1 = $(".page" + pagesfxa).position().top;
	wt2 = $(".page" + pagesfxb).position().top;
	wthigh = $(".wrapper > div[class*='page']:last").position().top;
	wtlow = $(".wrapper > div[class*='page']:first").position().top;



	$( window ).resize(function() {
		wt1 = $(".page1").position().top;
		wt2 = $(".page2").position().top;
	});
	
	currentScroll = $(window).scrollTop();
	savedScroll = currentScroll;
	
	
		console.log("on load. pagetotal: " + pagetotal + " wt1: " + wt1 + " wt2: " + wt2 + " wthigh: " + wthigh + " wtlow: " + wtlow + " currentScroll: " + currentScroll + " savedScroll: " + savedScroll );

	
	
	$(window).scroll(function(){
		 currentScroll = $(this).scrollTop();
	});	

	$.fn.scrollStopped = function(callback) {           
        $(this).scroll(function(){
        	var self = this, $this = $(self);
            if ($this.data('scrollTimeout')) 
				{clearTimeout($this.data('scrollTimeout'));}
            $this.data('scrollTimeout', setTimeout(callback,250,self));
        });
    };
	
	$(window).scrollStopped(function(){
	
	console.log("When scrolling stopped. currentScroll: " + currentScroll + " savedScroll: " + savedScroll );
	
	$(function(){
			wintop = wt1;
			what = wt2;
			
			if(currentScroll > savedScroll)
				{what = wt2;
				pagesfxa = pagesfxb;
				pagesfxb = pagesfxb + 1;				
				
				console.log("new values when scrolling down  wt1: " + wt1 + "wt2: " + wt2 + " pagesfxa: " + pagesfxa + " pagesfxb: " + pagesfxb + " so next scroll down will snap to .page" + pagesfxb);
				}
				else if ((currentScroll > savedScroll) && (wthigh = savedScroll)) {
					wt2 = wthigh;
					}
					
					else if ((currentScroll < savedScroll) && (wthigh < savedScroll)) {
					wt2 = wthigh;
					}
				
			else if(currentScroll < savedScroll)
				{what = wt1;}
			
			wintop = what;
			savedScroll = currentScroll;
		
			console.log("after if statements: currentScroll: " + currentScroll + " savedScroll: " + savedScroll + " wintop: " + wintop +  " wt1: " + wt1 + " wt2: " + wt2);

			$('html,body').animate(
				{scrollTop: wintop}
			);
		});

	});*/