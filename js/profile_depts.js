$(document).ready(function() {

	selectedProfile = window.location.hash.substring(1);
	$.each(companyDirectory, function(index, value) {
		if(value.href == selectedProfile && value.category == "Department"){
// APPEND DEPARTMENT INFO FROM DATA
			$('.dName').text( value.value );
			$('.mainImage').attr('src', 'images/' +  value.img );
			$('#dContent').text( value.value + " " + "Content for Department - long or short paragraph. Whatever is in the data. Lorem ipsum dolor sit amet, lectus sed vestibulum vel dolor nullam eu, non suscipit interdum suspendisse ac duis condimentum, non tincidunt accumsan, ut massa pede, odio adipiscing neque risus lacus eget dictum. Turpis nisl est non metus, pede lacus ipsum facilisi vulputate nullam. Adipiscing ac occaecati aliquam suspendisse ducimus, mauris velit per ut donec fugiat tincidunt, velit non ultrices et adipiscing lectus et." );
			$('#dMembers').text( value.value + " " + "Members" );
			$('#dFeed').text( value.value + " " + "Feed");
// APPEND LIST ITEMS FOR ALL COWORKER IMAGES AND LINKS TO PROFILES        
			$.each(companyDirectory, function(index, value) {
				if ( value.dept == $('.dName').first().text() ) {
					var deptsList = '<li class="content" style="float:left"><a class="imagelink btnRefresh" href="d_profile.html#' + value.href + '"><img class="roundPhoto" src="images/' + value.img + '" style="width:40px;"></a></li>';
					$('#deptsList').append(deptsList); 
				}
			});
		}
	});

});