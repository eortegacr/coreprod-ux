$(window).load(function() {

	$(".welcomingPeersBtn").click(function(){
		if($('.welcomingPeers').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.welcomingPeers').parent('.mainOverlay').slideDown(800);
			$('.welcomingPeers').fadeIn(1000);
			return false;
		}
	});

});