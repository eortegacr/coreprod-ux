$(window).load(function(){

	$('.announcementBtn').click(function(){
		if($('.announcements').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.announcements').parent('.mainOverlay').slideDown(800);
			$('.announcements').fadeIn(1000);
			return false;
		}
	});

	$('#sayThanksButton').click(function () {
		$('#sayThanks').slideToggle('slow');
	});

	$('#thanksMessageForm').submit(function(){
		$('#thanksMessage').hide();
		$('#thanksThanks').show();
		return false;
	});

	countAnnouncement = $('.announcementslide').size();
	currentAnnouncement = 1;
	nextAnnouncement = 2;
	prevAnnouncement = countAnnouncement;

	$('.nextAnnounce').click(function () {
		if (nextAnnouncement > countAnnouncement){
			nextAnnouncement = 1;
		}
		if (currentAnnouncement == 0){
			currentAnnouncement = countAnnouncement;
		}
		if (prevAnnouncement == 0){
			prevAnnouncement = countAnnouncement;
		}
		$('.announcement' + currentAnnouncement).hide();
		$('.announcement' + nextAnnouncement).fadeIn('slow');
		prevAnnouncement = nextAnnouncement - 1;
		currentAnnouncement = nextAnnouncement;
		nextAnnouncement = nextAnnouncement + 1;
		$('.pageAnnounce').empty();
		$('.pageAnnounce').text(currentAnnouncement);
	});

	$('.prevAnnounce').click(function () {
		if (nextAnnouncement > countAnnouncement){
			nextAnnouncement = 1;
		}
		if (currentAnnouncement == 0){
			currentAnnouncement = countAnnouncement;
		}
		if (prevAnnouncement == 0){
			prevAnnouncement = countAnnouncement;
		}
		$('.announcement' + currentAnnouncement).hide();
		$('.announcement' + prevAnnouncement).fadeIn('slow');
		currentAnnouncement = prevAnnouncement;
		nextAnnouncement = prevAnnouncement + 1;
		prevAnnouncement = prevAnnouncement - 1;
		$('.pageAnnounce').empty();
		$('.pageAnnounce').text(currentAnnouncement);
	});

	$('.pageAnnounce').empty();
	$('.pageAnnounce').text(currentAnnouncement);

	var total = $('.totalAnnounce');
	total.text(countAnnouncement)

	announcementCount = $(".announcementslide").size();
	if(announcementCount  > 0){
		$('.mainOverlay').slideUp(500);
		$('.mainOverlay > div').fadeOut(500);
		$('.announcements').parent('.mainOverlay').slideDown(800);
		$('.announcements').fadeIn(1000);
		return false;
	} 
	else {
		$("h5:has(a[class='announcementBtn'])").hide();
	}

});