$(document).ready(function() {
	$("#languageSelect").click(function(){
		$(".mainOverlay").slideDown(1000);
		$("#regionSelect").fadeIn(1500);
	});
	$("#passwordSelect").click(function(){
		$(".mainOverlay").slideDown(1000);
		$("#passwordReset").fadeIn(1500);
	});
	$("#registerSelect").click(function(){
		$(".mainOverlay").slideDown(1000);
		$("#firstLogin").fadeIn(1500);	
	});
});

$(function() {
	$("#accordion").accordion({
		heightStyle: "content"
	});
	$(".selector").accordion({ header: "h5" });
});


/*$().ready(function() {
	$("#loginForm").validate({
		errorLabelContainer: $("#loginForm div.error")
	});
	var container = $('div.container');
});*/