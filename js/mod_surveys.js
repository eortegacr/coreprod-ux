$(document).ready(function() {

	setInterval(function(){
		var beginDate = new Date("3/21/2015");
		var todayDate = new Date();
	
		if(todayDate < beginDate){
			var todayHrs = (23)-(todayDate.getHours());
			var todayMin = (59)-(todayDate.getMinutes());
			var todaySec = (59)-(todayDate.getSeconds());
	
			var timeDiff = Math.abs(todayDate.getTime() - beginDate.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	
			$('.durationDay').text(diffDays-1);
			$('.durationHr').text(todayHrs);
			$('.durationMin').text(todayMin);
			$('.durationSec').text(todaySec);
		}
	}, 1000);

//START NEXT/PREV FUNCTIONALITY
	countSurvQ = $('.surveyQuestion').size();
	currentSurvQ = 1;
	nextSurvQ = 2;
	prevSurvQ = countSurvQ;
	$('.prevSurvQ, .surveyControlBreaker').hide();

	$('.nextSurvQ').click(function () {
		$('#surveySlide' + currentSurvQ).hide();
		$('#surveySlide' + nextSurvQ).fadeIn('slow');
		prevSurvQ = nextSurvQ - 1;
		currentSurvQ = nextSurvQ;
		nextSurvQ = nextSurvQ + 1;
		$('.pageSurvQ').empty();
		$('.pageSurvQ').text(currentSurvQ);
			if (nextSurvQ > countSurvQ){
			$('#mainSurvey').hide();
			$('.surveyCompleteConfirm').show()
		}
		if (currentSurvQ == 0){
			currentSurvQ = countSurvQ;
		}
		if (currentSurvQ >= 1){
			$('.prevSurvQ, .surveyControlBreaker').show();
		}
		if (prevSurvQ == 0){
			prevSurvQ = countSurvQ;
		}
		return false;
	});

	$('.prevSurvQ').click(function () {
		$('#surveySlide' + currentSurvQ).hide();
		$('#surveySlide' + prevSurvQ).fadeIn('slow');
		currentSurvQ = prevSurvQ;
		nextSurvQ = prevSurvQ + 1;
		prevSurvQ = prevSurvQ - 1;
		$('.pageSurvQ').empty();
		$('.pageSurvQ').text(currentSurvQ);
		if (nextSurvQ > countSurvQ){
			nextSurvQ = 1;
		}
		if (currentSurvQ == 0){
			currentSurvQ = countSurvQ;
		}
		if (currentSurvQ <= 1){
			$('.prevSurvQ, .surveyControlBreaker').hide();
		}
		if (prevSurvQ == 0){
			prevSurvQ = countSurvQ;
		}
		return false;
	});

	$('.pageSurvQ').empty();
	$('.pageSurvQ').text(currentSurvQ);

	var total = $('.sizeSurvQ');
	total.text(countSurvQ);
});

	$(function () {
		$('#surveyAnsChart').highcharts({
			credits: {enabled: false},
			navigation: {
				buttonOptions: {enabled: false}
			},
			colors: ['#777', '#2ad'],
			chart: {type: 'bar'},
			title: {text: null},
			xAxis: {
				categories: ['I know what is expected of me at work.', 'I have the materials and equipment I need to do my work right.', 'At work, I have the opportunity to do what I do best every day.', 'In the past seven days, I have received recognition or praise for good work.', 'My supervisor, or someone at work, seems to care about me as a person.', 'There is someone at work who encourages my development.', 'At work, my opinions seem to count.', 'The mission or purpose of my company makes me feel my job is important.', 'My associates or fellow employees are committed to doing quality work.', 'I have a best friend at work.', 'In the past six months, someone at work has talked to me about my progress.', 'In the past year, I have had opportunities at work to learn and grow. ']
			},
			yAxis: {
				min: 0,
				title: {text: null}
			},
			legend: {reversed: true},
			plotOptions: {
				series: {stacking: 'normal'}
			},
			series: [
				{
				name: 'False',
				data: [['Others Answered', 20],['Others Answered', 20],['Others Answered', 30],['Others Answered', 20],['Others Answered', 10],['Others Answered', 50],['Top Answer', 70],['Others Answered', 25],['Top Answer', 75],['Others Answered', 45],['Your Answer is the Top Answer', 90],['Top Answer', 60]]
				},
				{
				name: 'True',
				data: [['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 70], ['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 90], ['Your Answer', 50], ['Your Answer', 30], ['Your Answer is the Top Answer', 75], ['Your Answer', 25], ['Your Answer is the Top Answer', 55], ['Others Answered', 10], ['Your Answer', 40]]
				}
			]
		});
	});

	$(window).load(function(){
		$('.surveyResultsBtn').click(function(){
			if($('.surveyResults').css('display') == 'block'){
				$('.mainOverlay').slideUp(500);
				$('.mainOverlay > div').fadeOut(500);
				return false;
			}
			else{
				$('.mainOverlay').slideUp(500);
				$('.mainOverlay > div').fadeOut(500);
				$('.surveyResults').parent('.mainOverlay').slideDown(800);
				$('.surveyResults').fadeIn(1000);
				return false;
			}
		});
	});