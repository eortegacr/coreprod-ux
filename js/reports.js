//LOGIN CHART
$(function () {
	$('#loginChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {
			type: 'area',
			zoomType: 'x'
		},
		plotOptions: {series: {fillOpacity: 0}},
		title: {text: 'Logins'},
		xAxis: {
			title: {text: 'Date'},
			categories: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
		},
		yAxis: {
			title: {text: 'Number of Logins'}
		},
		tooltip: {
			crosshairs: true,
			shared: true,
			valueSuffix: ''
		},
		legend: {enabled: false},
		series: [
			{
				name: 'All Logins',
				data: [10, 20, 30, 25, 40, 16, 35, 46, 40, 25, 20, 40],
				color: '#2AD',
				fillColor: 'rgba(34, 170, 221, 0.5)',
			},
			{
				name: 'Unique Logins',
				data: [5, 10, 3, 16, 24, 14, 5, 3, 19, 15, 10, 20],
				color: '#8C4',
				fillColor: 'rgba(136, 204, 68, 0.5)',
			}
		]
	});
});

// REGISTERED USERS CHART
$(function () {
	$('#regUsersChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {
			type: 'area',
			zoomType: 'x'
		},
		plotOptions: {series: {fillOpacity: 0}},
		title: {text: ''},
		xAxis: {
			title: {text: 'Date'},
			categories: ['Jan','Feb','Mar','Apr']
		},
		yAxis: {title: {text: 'Amount of Users'}},
		tooltip: {
			crosshairs: true,
			shared: true,
			valueSuffix: ''
		},
		legend: {enabled: false},
		series: [
			{
				name: 'Total Users',
				data: [10, 20, 30, 25],
				color: '#2AD',
				fillColor: 'rgba(34, 170, 221, 0.5)',
			},
			{
			name: 'New Users',
			data: [5, 10, 3, 16],
			color: '#8C4',
			fillColor: 'rgba(136, 204, 68, 0.5)',
			}
		]
	});
});

// SURVEY RESULTS CHARTS
$(function () {
	$('#surveyResultsChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		navigation: {buttonOptions: {enabled: false}},
		chart: {type: 'bar'},
		title: {text: null},
		xAxis: {
			categories: ['I know what is expected of me at work.', 'I have the materials and equipment I need to do my work right.', 'At work, I have the opportunity to do what I do best every day.', 'In the past seven days, I have received recognition or praise for good work.', 'My supervisor, or someone at work, seems to care about me as a person.', 'There is someone at work who encourages my development.', 'At work, my opinions seem to count.', 'The mission or purpose of my company makes me feel my job is important.', 'My associates or fellow employees are committed to doing quality work.', 'I have a best friend at work.', 'In the past six months, someone at work has talked to me about my progress.', 'In the past year, I have had opportunities at work to learn and grow. ']
		},
		yAxis: {
			min: 0,
			title: {text: null}
		},
		legend: {reversed: true},
		plotOptions: {series: {stacking: 'normal'}},
		series: [
			{
				name: 'False',
				data: [['Others Answered', 20],['Others Answered', 20],['Others Answered', 30],['Others Answered', 20],['Others Answered', 10],['Others Answered', 50],['Top Answer', 70],['Others Answered', 25],['Top Answer', 75],['Others Answered', 45],['Your Answer is the Top Answer', 90],['Top Answer', 60]]
			},
			{
				name: 'True',
				data: [['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 70], ['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 90], ['Your Answer', 50], ['Your Answer', 30], ['Your Answer is the Top Answer', 75], ['Your Answer', 25], ['Your Answer is the Top Answer', 55], ['Others Answered', 10], ['Your Answer', 40]]
			}
		]
	});
});

//TRAINING COMPLETION CHARTS
$(function () {
	$('#trainingCompletionChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		navigation: {buttonOptions: {enabled: false}},
		chart: {type: 'bar'},
		title: {text: null},
		xAxis: {
			categories: ['I know what is expected of me at work.', 'I have the materials and equipment I need to do my work right.', 'At work, I have the opportunity to do what I do best every day.', 'In the past seven days, I have received recognition or praise for good work.', 'My supervisor, or someone at work, seems to care about me as a person.', 'There is someone at work who encourages my development.', 'At work, my opinions seem to count.', 'The mission or purpose of my company makes me feel my job is important.', 'My associates or fellow employees are committed to doing quality work.', 'I have a best friend at work.', 'In the past six months, someone at work has talked to me about my progress.', 'In the past year, I have had opportunities at work to learn and grow. ']
		},
		yAxis: {
			min: 0,
			title: {text: null}
		},
		legend: {reversed: true},
		plotOptions: {series: {stacking: 'normal'}},
		series: [
			{
				name: 'False',
				data: [['Others Answered', 20],['Others Answered', 20],['Others Answered', 30],['Others Answered', 20],['Others Answered', 10],['Others Answered', 50],['Top Answer', 70],['Others Answered', 25],['Top Answer', 75],['Others Answered', 45],['Your Answer is the Top Answer', 90],['Top Answer', 60]]
			},
			{
				name: 'True',
				data: [['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 70], ['Your Answer is the Top Answer', 80], ['Your Answer is the Top Answer', 90], ['Your Answer', 50], ['Your Answer', 30], ['Your Answer is the Top Answer', 75], ['Your Answer', 25], ['Your Answer is the Top Answer', 55], ['Others Answered', 10], ['Your Answer', 40]]
			}
		]
	});
});

// APPROVED RECOGNITION COMPARISON CHART
$(function () {
	$('#approvedRec').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: 0,
			plotShadow: false
		},
		plotOptions: {series: {fillOpacity: 0}},
		title: {text: ''},
		tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'}
				}
			}
		},
		series: [{
			type: 'pie',
			name: 'Award Type',
			data: [
				['Thumbs Up', 30.0],
				['Techy Master', 50.0],
				['Fund Raiser',    20.0]
			]
		}]
	});
});

// BUDGET CHART
$(function () {
	var categories = ['Customer Success', 'Finance', 'Sales', 'IT', 'Marketing'];
	$('#budgetGlobal').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'bar'},
		title: {text: ''},
		subtitle: {text: ''},
		xAxis: [
			{
				categories: categories,
				reversed: false,
				labels: {step: 1}
			},
			{
				opposite: true,
				reversed: false,
				categories: categories,
				linkedTo: 0,
				labels: {step: 1}
			}
		],
		yAxis: {
			title: {text: null},
			min: -100,
			max: 100
		},
		plotOptions: {series: {stacking: 'normal'}},
		tooltip: {
			formatter: function () {
				return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
			}
		},
		series: [
			{
				name: 'Used',
				data: [-10, -40, -20, -50, -20]
			},
			{
				name: 'Unused',
				data: [20, 80, 60, 10, 30]
			}
		]
	});
});

// BUDGET CHART
$(function () {
	var categories = ['Customer Success', 'Finance', 'Sales', 'IT', 'Marketing'];
	$('#budgetDirect').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'bar'},
		title: {text: ''},
		subtitle: {text: ''},
		xAxis: [
			{
				categories: categories,
				reversed: false,
				labels: {
					step: 1
				}
			},
			{
				opposite: true,
				reversed: false,
				categories: categories,
				linkedTo: 0,
				labels: {
					step: 1
				}
			}
		],
		yAxis: {
			title: {text: null},
			min: -100,
			max: 100
		},
		plotOptions: {series: {stacking: 'normal'}},
		tooltip: {
			formatter: function () {
				return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
					'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
			}
		},
		series: [
			{
				name: 'Used',
				data: [-10, -40, -20, -50, -20]
			},
			{
				name: 'Unused',
				data: [20, 80, 60, 10, 30]
			}
		]
	});
});

// COST PER RECOGNITION CHART
$(function () {
	$('#cost_per_rec').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {	type: 'bar'},
		title: {	text: ''	},
		subtitle: {text: ''},
		xAxis: {
			categories: ['Thumbs Up', 'Techy Master', 'Fund Raiser'],
			title: {	text: null}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Total Cost',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		plotOptions: {bar: {dataLabels: {enabled: true}}},
		series: [
			{
				name: 'Total Cost ($)',
				data: [60, 80, 110],
				color: '#2AD'
			}
		]
	});
});

// MILESTONES CHART
$(function () {
	$('#milestonesChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {
			type: 'heatmap',
			marginTop: 40,
			marginBottom: 40
		},
		title: {	text: ''	},
		xAxis: {	categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],},
		yAxis: {	
			categories: ['Anniversary', 'Birthday', 'New Employee'],
			title: null
		},
		colorAxis: {
			min: 0,
			minColor: '#FFFFFF',
			maxColor: Highcharts.getOptions().colors[0]
		},
		legend: {
			align: 'right',
			layout: 'vertical',
			margin: 0,
			verticalAlign: 'top',
			y: 25,
			symbolHeight: 320
		},
		tooltip: {
			formatter: function () {
				return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> there are <br><b>' + this.point.value + '</b> <b> ' + this.series.yAxis.categories[this.point.y] + "'s</b>";
			}
		},
		series: [
			{
				name: '',
				borderWidth: 1,
				data: [[0, 0, 10], [0, 1, 19], [0, 2, 8], [1, 0, 92], [1, 1, 58], [1, 2, 78], [2, 0, 35], [2, 1, 15], [2, 2, 123], [3, 0, 72], [3, 1, 132], [3, 2, 114], [4, 0, 38], [4, 1, 5], [4, 2, 8]],
				dataLabels: {
					enabled: true,
					color: 'black',
					style: {
						textShadow: 'none',
						HcTextStroke: null
					}
				}
			}
		]
	});
});

// FIRST TIME ENGAGEMENT CHART
$(function () {
	$('#firstTimeEngChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {	type: 'column'},
		title: {	text: ''},
		xAxis: {	categories: ['Customer Success', 'Finance', 'IT', 'Marketing', 'Sales']},
		yAxis: {
			min: 0,
			title: {	text: 'Total New Employees'},
			stackLabels: {
				enabled: true,
				style: {
					fontWeight: 'bold',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				}
			}
		},
		legend: {
			align: 'right',
			x: -70,
			verticalAlign: 'top',
			y: 20,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			borderColor: '#CCC',
			borderWidth: 1,
			shadow: false
		},
		tooltip: {
			formatter: function () {
				return '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '<br/>' + 'Total: ' + this.point.stackTotal;
			}
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: true,
					color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
					style: {textShadow: '0 0 3px black, 0 0 3px black'}
				}
			}
		},
		series: [
			{
				name: 'Recognized',
				data: [5, 3, 4, 7, 2],
				color: '#2AD'
			},
			{
				name: 'Unrecognized',
				data: [2, 2, 3, 2, 1],
				color: '#88CC44'
			}
		]
	});
});

// PAGES / CLICKS CHART
$(function () {
	$('#pagesClicksChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {
			plotBackgroundColor: null,
			plotShadow: false
		},
		title: {text: ''},
		tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'}
				}
			}
		},
		series: [
			{
				type: 'pie',
				name: '',
				data: [
					['Home', 50],
					['Mail', 20],
					{
						name: 'Training',
						y: 120,
						sliced: true,
						selected: true
					},
					['Promotion', 70],
					['Reports', 90],
					['Admin', 200],
					['Help', 10]
				]
			}
		]
	});
});

// REDEMPTIONS GLOBAL CHART
$(function () {
	$('#redemptionsGlobalChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'pie'},
		plotOptions: {series: {fillOpacity: 0}},
		title: {	text: ''	},
		subtitle: {text: 'Click the slices to view versions.'},
		plotOptions: {
			series: {
				dataLabels: {
					enabled: true,
					format: '{point.name}: {point.y:.1f}%'
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
		},
		series: [
			{
				name: 'Redemptions',
				colorByPoint: true,
				data: [
					{
						name: 'Multi-Store Card',
						y: 5,
					},
					{
						name: 'Single-Store Card',
						y: 25,
						drilldown: 'Single-Store Card'
					},
					{
						name: 'Instant Redemption',
						y: 50,
						drilldown: 'Instant Redemption'
					},
					{
						name: 'On Demand',
						y: 20,
						drilldown: 'On Demand'
					}
				]
			}
		],
		drilldown: {
			series: [
				{
					id: 'Single-Store Card',
					data: [
						['Macys', 4],
						['Target', 2],
						['JC Penney', 3],
						['Coach', 1],
						['Walgreens', 3]
					]
				},
				{
					id: 'Instant Redemption',
					data: [
						['1-800-CATS', 4],
						['1-800-FLOWERS', 2],
						['The Sports Authority', 5]
					]
				},
				{
					id: 'On Demand',
					data: [
						['Buffalo Wild Wings', 4],
						['Golfsmith', 2],
						['Bass Pro Shops', 7],
						['Applebees', 5],
						['Sephora', 3]
					]
				}
			]
		}
	});
});

// REDEMPTIONS PUBLIC CHART
$(function () {
	$('#redemptionsPublicChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {	type: 'pie'},
		plotOptions: {series: {fillOpacity: 0}},
		title: {text: ''},
		subtitle: {text: 'Click the slices to view versions.'},
		plotOptions: {
			series: {
				dataLabels: {
					enabled: true,
					format: '{point.name}: {point.y:.1f}%'
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
		},
		series: [
			{
				name: 'Redemptions',
				colorByPoint: true,
				data: [
					{
						name: 'Multi-Store Card',
						y: 5,
					},
					{
						name: 'Single-Store Card',
						y: 25,
						drilldown: 'Single-Store Card'
					},
					{
						name: 'Instant Redemption',
						y: 50,
						drilldown: 'Instant Redemption'
					},
					{
						name: 'On Demand',
						y: 20,
						drilldown: 'On Demand'
					}
				]
			}
		],
		drilldown: {
			series: [
				{
					id: 'Single-Store Card',
					data: [
						['Macys', 4],
						['Target', 2],
						['JC Penney', 3],
						['Coach', 1],
						['Walgreens', 3]
					]
				},
				{
					id: 'Instant Redemption',
					data: [
						['1-800-CATS', 4],
						['1-800-FLOWERS', 2],
						['The Sports Authority', 5]
					]
				},
				{
					id: 'On Demand',
					data: [
						['Buffalo Wild Wings', 4],
						['Golfsmith', 2],
						['Bass Pro Shops', 7],
						['Applebees', 5],
						['Sephora', 3]
					]
				}
			]
		}
	});
});

// RECOGNITION DURATION CHART
$(function () {
	$('#recDurationChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'column'},
		title: {text: ''},
		xAxis: {type: 'category'},
		legend: {enabled: false},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {enabled: true}
			}
		},
		series: [
			{
				name: 'Departments',
				colorByPoint: true,
				data: [
					{
						name: 'Customer Success',
						y: 5,
						drilldown: 'Customer Success'
					},
					{
						name: 'Finance',
						y: 2,
						drilldown: 'Finance'
					},
					{
						name: 'IT',
						y: 4,
						drilldown: 'IT'
					},
					{
						name: 'Marketing',
						y: 2,
						drilldown: 'Marketing'
					},
					{
						name: 'Sales',
						y: 2,
						drilldown: 'Sales'
					}
				]
			}
		],
		drilldown: {
			series: [
				{
					id: 'Customer Success',
					data: [
						['Project Management', 4],
						['Customer Service', 2]
					]
				},
				{
					id: 'Finance',
					data: [
						['Accounting', 4],
						['Collections', 2]
					]
				},
				{
					id: 'IT',
					data: [
						['UX Development', 4],
						['Graphic Design', 2],
						['Cold Fusion Development', 2],
						['Quality Assurance', 2]
					]
				},
				{
					id: 'Marketing',
					data: [
						['Advertising', 4],
						['SEO', 2]
					]
				},
				{
					id: 'Sales',
					data: [
						['Sales Force', 4],
						['Sales', 2]
					]
				}
			]
		}
	});
});

// PEER TO PEER CHART
$(function () {
	$('#p2pChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'column'},
		title: {text: ''},
		xAxis: {type: 'category'},
		legend: {enabled: false},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true
				}
			}
		},
		series: [
			{
				name: 'Departments',
				colorByPoint: true,
				data: [
					{
						name: 'Customer Success',
						y: 5,
						drilldown: 'Customer Success'
					},
					{
						name: 'Finance',
						y: 2,
						drilldown: 'Finance'
					},
					{
						name: 'IT',
						y: 4,
						drilldown: 'IT'
					},
					{
						name: 'Marketing',
						y: 2,
						drilldown: 'Marketing'
					},
					{
						name: 'Sales',
						y: 2,
						drilldown: 'Sales'
					}
				]
			}
		],
		drilldown: {
			series: [
				{
					id: 'Customer Success',
					data: [
						['Project Management', 4],
						['Customer Service', 2]
					]
				},
				{
					id: 'Finance',
					data: [
						['Accounting', 4],
						['Collections', 2]
					]
				},
				{
					id: 'IT',
					data: [
						['UX Development', 4],
						['Graphic Design', 2],
						['Cold Fusion Development', 2],
						['Quality Assurance', 2]
					]
				},
				{
					id: 'Marketing',
					data: [
						['Advertising', 4],
						['SEO', 2]
					]
				},
				{
					id: 'Sales',
					data: [
						['Sales Force', 4],
						['Sales', 2]
					]
				}
			]
		}
	});
});

// PENDING RECOGNITIONS CHART
$(function () {
	$('#pendingRecChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'column'},
		title: {text: ''},
		xAxis: {type: 'category'},
		legend: {enabled: false},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true
				}
			}
		},
		series: [
			{
				name: 'Departments',
				colorByPoint: true,
				data: [
					{
						name: 'Customer Success',
						y: 5,
						drilldown: 'Customer Success'
					},
					{
						name: 'Finance',
						y: 2,
						drilldown: 'Finance'
					},
					{
						name: 'IT',
						y: 4,
						drilldown: 'IT'
					},
					{
						name: 'Marketing',
						y: 2,
						drilldown: 'Marketing'
					},
					{
						name: 'Sales',
						y: 2,
						drilldown: 'Sales'
					}
				]
			}
		],
		drilldown: {
			series: [
				{
					id: 'Customer Success',
					data: [
						['Project Management', 4],
						['Customer Service', 2]
					]
				},
				{
					id: 'Finance',
					data: [
						['Accounting', 4],
						['Collections', 2]
					]
				},
				{
					id: 'IT',
					data: [
						['UX Development', 4],
						['Graphic Design', 2],
						['Cold Fusion Development', 2],
						['Quality Assurance', 2]
					]
				},
				{
					id: 'Marketing',
					data: [
						['Advertising', 4],
						['SEO', 2]
					]
				},
				{
					id: 'Sales',
					data: [
						['Sales Force', 4],
						['Sales', 2]
					]
				}
			]
		}
	});
});

// TRANSACTIONS GLOBAL CHART
$(function () {
	$('#transactionsGlobalChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'column'},
		title: {text: ''},
		xAxis: {type: 'category'},
		legend: {enabled: false},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {enabled: true}
			}
		},
		series: [
			{
				name: 'Departments',
				colorByPoint: true,
				data: [
					{
						name: 'Customer Success',
						y: 5,
						drilldown: 'Customer Success'
					},
					{
						name: 'Finance',
						y: 2,
						drilldown: 'Finance'
					},
					{
						name: 'IT',
						y: 4,
						drilldown: 'IT'
					},
					{
						name: 'Marketing',
						y: 2,
						drilldown: 'Marketing'
					},
					{
						name: 'Sales',
						y: 2,
						drilldown: 'Sales'
					}
				]
			}
		],
		drilldown: {
			series: [
				{
					id: 'Customer Success',
					data: [
						['Project Management', 4],
						['Customer Service', 2]
					]
				},
				{
					id: 'Finance',
					data: [
						['Accounting', 4],
						['Collections', 2]
					]
				},
				{
					id: 'IT',
					data: [
						['UX Development', 4],
						['Graphic Design', 2],
						['Cold Fusion Development', 2],
						['Quality Assurance', 2]
					]
				},
				{
					id: 'Marketing',
					data: [
						['Advertising', 4],
						['SEO', 2]
					]
				},
				{
					id: 'Sales',
					data: [
						['Sales Force', 4],
						['Sales', 2]
					]
				}
			]
		}
	});
});

// TRANSACTIINS PUBLIC CHART
$(function () {
	$('#transactionsPublicChart').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {type: 'column'},
		title: {	text: ''},
		xAxis: {	type: 'category'	},
		legend: {enabled: false},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true
				}
			}
		},
		series: [
			{
				name: 'Departments',
				colorByPoint: true,
				data: [
					{
						name: 'Thumbs Up',
						y: 5,
					},
					{
						name: 'Techy Master',
						y: 2,
					},
					{
						name: 'Fund Raiser',
						y: 12,
					}
				]
			}
		],
	});
});

// BROWSER USAGE CHART
$(function () {
	$('#browser_use').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {	type: 'pie'},
		plotOptions: {series: {fillOpacity: 0}},
		title: {	text: ''},
		subtitle: {text: 'Click the slices to view versions.'},
		plotOptions: {
			series: {
				dataLabels: {
					enabled: true,
					format: '{point.name}: {point.y:.1f}%'
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
		},
		series: [
			{
				name: 'Browser',
				colorByPoint: true,
				data: [
					{
						name: 'Internet Explorer',
						y: 5,
						drilldown: 'Internet Explorer'
					},
					{
						name: 'Firefox',
						y: 2,
						drilldown: 'Firefox'
					},
					{
						name: 'Chrome',
						y: 4,
						drilldown: 'Chrome'
					},
					{
						name: 'Safari',
						y: 4,
						drilldown: 'Safari'
					},
					{
						name: 'Opera',
						y: 4,
						drilldown: 'Opera'
					},
					{
						name: 'Other',
						y: 4,
						drilldown: 'Other'
					}
				]
			}
		],
		drilldown: {
			series: [
				{
					id: 'Internet Explorer',
					data: [
						['IE v7', 4],
						['IE v8', 2],
						['IE v9', 1],
						['IE v10', 2],
						['IE v11', 1]
					]
				},
				{
					id: 'Firefox',
					data: [
						['Firefox v33', 4],
						['Firefox v34', 2]
					]
				},
				{
					id: 'Safari',
					data: [
						['Safari v6', 4],
						['Safari v7', 2],
						['Safari v8', 5]
					]
				},
				{
					id: 'Opera',
					data: [
						['Opera v25', 4],
						['Opera v26', 2]
					]
				},
				{
					id: 'Other',
					data: [
						['Browser', 7],
						['Browser', 4]
					]
				},
				{
					id: 'Chrome',
					data: [
						['Chrome v38', 4],
						['Chrome v39', 11],
						['Chrome v40', 2]
					]
				}
			]
		}
	});
});

// MOBILE LOGINS CHART
$(function () {
	$('#mobileLogins').highcharts({
		colors: companyColors,
		credits: {enabled: false},
		chart: {
			plotBackgroundColor: null,
			plotShadow: false
		},
		title: {text: ''},
		tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'}
				}
			}
		},
		series: [
			{
			type: 'pie',
			name: 'Mobile Device',
			data: [
					['Android',   45.0],
					['Blackberry',       26.8],
					{
						name: 'IOS',
						y: 12.8,
						sliced: true,
						selected: true
					},
					['Windows',    8.5],
					['Others',   0.7]
				]
			}
		]
	});
});

// SHOW/HIDE HIGHCHART BUTTON
$(document).ready(function () {

	$(document).on('click', '.displayChart', function () {
		$(this).next().slideToggle('slow');
		if ( $(this).text() == 'Hide Chart' ) {
			$(this).text('Display Chart');
		}
		else {
			$(this).text('Hide Chart');
		}
	});

	var $select = $('#redemptionVendors');
	$select.find('option').remove();
	$select.append('<option value="">Select Vendor...</option>');
	$.each(merchantData.DATA,function(key, value) {
		$select.append('<option value=' + key + '>' + value[1] + '</option>');
	});

	function exportTableToCSV($table, filename) {
		var $rows = $table.find('tr:has(td)'),
			tmpColDelim = String.fromCharCode(11),
			tmpRowDelim = String.fromCharCode(0),
			colDelim = '","',
			rowDelim = '"\r\n"',
			csv = '"' + $rows.map(function (i, row) {
				var $row = $(row),
					$cols = $row.find('td');
				return $cols.map(function (j, col) {
					var $col = $(col),
					text = $col.text();
					return text.replace('"', '""');
				}).get().join(tmpColDelim);
			}).get().join(tmpRowDelim).split(tmpRowDelim).join(rowDelim).split(tmpColDelim).join(colDelim) + '"',
			csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

		$(this)
			.attr({
				'download': filename,
				'href': csvData,
				'target': '_blank'
		});
	}

	$(".export").on('click', function (event) {
		exportTableToCSV.apply(this, [$('#dvData>table'), 'export.csv']);
	});

});

$(window).load(function(){

// Append changes in update budget modal to the table on budget reports page
	$('#updateBudgetForm').submit(function () {
		var total = $('.active > td.total').html();
		var used = $('.active > td.usedBudget').html();
		var unused = total - used;
		var inputNum = $('#budgetInput').val().trim();
		var usedNum = $('.active > td.usedBudget').html().trim();
		if ( parseInt(inputNum) >= parseInt(usedNum) ) {
			$('.active > td.total').html($('#budgetInput').val());
// Subtract used from new total to get unused values
			$('.active > td.unusedBudget').html($('.active > td.total').html() - $('.active > td.usedBudget').html());
// When submitting new budget, clear input and close overlay
			$('#budgetInput').val('');
			$(this).parents('.mainOverlay').slideUp(500);
// If input new budget num is < used budget, alert user budget must be > used budget
		}
		else {
			alert('Budget must be higher than what this department has already used.'); 
			$('#budgetInput').val('');
		}
		//return false;
	});

//APPEARS IN MILESTONE MODULE AND UPCOMING MILESTONES REPORT
	$('.milestoneCongrateReportCTA').click(function(){
		var peerName= $(this).parents('tr').find('.milestoneEmpName').text().trim();
		$('.peerName').empty();
		$('.peerName').text(peerName);
		if($('.milestoneCongrate').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.milestoneCongrate').parent('.mainOverlay').slideDown(800);
			$('.milestoneCongrate').fadeIn(1000);
			return false;
		}
	});
	$('.highlightTransactionBtn').click(function(){
		if($('.highlightTransaction').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.highlightTransaction').parent('.mainOverlay').slideDown(800);
			$('.highlightTransaction').fadeIn(1000);
			return false;
		}
	});

	$('.pendingRecCTA').click(function(){
		if($('.approvalForm').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.approvalForm').parent('.mainOverlay').slideDown(800);
			$('.approvalForm').fadeIn(1000);
			return false;
		}
	});

	$( "#changeRecLevel" ).change(function () {
			var imgstr = "";
			var recTitlestr = "";
			var recValuestr = "";
			$( "#changeRecLevel option:selected" ).each(function() {
				imgstr += $( this ).val();
				recTitlestr += $( this ).text().split(' | ')[0];
				recValuestr += $( this ).text().split(' | ')[1];
			});
			$( ".pendingRecLevel > img" ).attr('src','images/reward_logo_' + imgstr + '_120x120.png');
			$( ".pendingRecLevel > h4" ).text(recTitlestr);
			$( ".pendingRecLevel > h5" ).text(recValuestr);
		}).change();

	$('.updateBudgetLink').click(function(){
		if($('.updateBudget').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.updateBudget').parent('.mainOverlay').slideDown(800);
			$('.updateBudget').fadeIn(1000);
			return false;
		}
	});

	$('.firstEngagementCongrateLink').click(function(){
		if($('.firstEngagementCongrate').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.firstEngagementCongrate').parent('.mainOverlay').slideDown(800);
			$('.firstEngagementCongrate').fadeIn(1000);
			return false;
		}
	});

	$('.sendfirstEngagementMessage').click(function(){
		$('.firstEngagementMessage').hide();
		$('.firstEngagementMessageConfirmed').show();
			return false;
	});

	$('.alertDurationLink').click(function(){
		if($('.alertDuration').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.alertDuration').parent('.mainOverlay').slideDown(800);
			$('.alertDuration').fadeIn(1000);
			return false;
		}
	});

	$('.sendDurAlert').click(function(){
		$('.durationMessage').hide();
		$('.durationMessageConfirmed').show();
		return false;
	});

	$('.alertWelcomeWagonLink').click(function(){
		if($('.alertWelcomeWagon').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
				return false;
			}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.alertWelcomeWagon').parent('.mainOverlay').slideDown(800);
			$('.alertWelcomeWagon').fadeIn(1000);
				return false;
			}
	});

	$('.sendWelcomeWagonAlert').click(function(){
		$('.welcomeWagonMessage').hide();
		$('.welcomeWagonMessageConfirmed').show();
			return false;
	});

	$('.alertLowLoginLink').click(function(){
		if($('.alertLowLogin').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
				return false;
		}
		else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.alertLowLogin').parent('.mainOverlay').slideDown(800);
			$('.alertLowLogin').fadeIn(1000);
				return false;
		}
	});

	$('.sendLowLoginAlert').click(function(){
		$('.lowLoginMessage').hide();
		$('.lowLoginMessageConfirmed').show();
			return false;
	});

// Get Budget Information from table in budget report, display in change budget modal
	$('.adjBudgetCTA').click(function () {
		$('tr').removeClass('active');
		$(this).parents('tr').addClass('active');
		$('#budgetChosenDept').text($(this).parents('tr').children().first().text());
		$('#budgetChosenCurrentTotal').text($(this).parents('tr').children().first().next().text());
		$('#budgetChosenCurrentUnused').text($(this).parents('tr').children().last().prev().text())
		$('#budgetChosenCurrentUsed').text($(this).parents('tr').children().last().prev().prev().text());
	});

});