$(document).ready( function () {

	$('.profileIcon').click(function(){
		icontitle= $('img', this).attr('title');
		if($(this).hasClass('interestIcon')){
			$('.icontitle').text('These peers also share an interest in '+icontitle);
		}
		if($(this).hasClass('badgeIcon')){
			$('.icontitle').text('These peers also have a '+icontitle);
		}
		if($(this).hasClass('skillIcon')){
			$('.icontitle').text('These peers are also skilled in '+icontitle);
		}
		if($('.profileIcons').css('display') == 'block'){
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			return false;
		}else{
			$('.mainOverlay').slideUp(500);
			$('.mainOverlay > div').fadeOut(500);
			$('.profileIcons').parent('.mainOverlay').slideDown(800);
			$('.profileIcons').fadeIn(1000);
			return false;
		}
	});

	$('.selectStat').change(function(){
		var myStatsTitle=$('option:selected',this).attr('name');
		$('.myStatsContent').hide();
		$('.myStatsContent:contains('+myStatsTitle+')').show();
	});

// Locate and select employees to reward
	$( "#lookupEmp" ).autocomplete({
		source: companyDirectory,
		minLength: 0,
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ){
		return $( "<li>" )
			.data('ui-autocomplete-item', item)
			.append("<a><img src='images/" + item.img + "'>" + item.value + " | <span class='listingtype'>" + item.category + "</span></a>" )
			.appendTo( ul );
	};
	$( "#lookupEmp" ).on( "autocompleteselect", function( event, ui ) {
		$('#lookupEmpResult').empty();
		if (ui.item.category == 'User'){
$('#lookupEmpResult').append("<h5>"+ui.item.value+"</h5><img src='images/"+ui.item.img+"' class='roundPhoto' /><br /><p><a href='d_profile.html#"+ui.item.href+"'>Visit Profile</a></p>");
		}
		else if (ui.item.category == 'Group'){
			$('#lookupEmpResult').append("<h5>"+ui.item.value+"</h5><img src='images/"+ui.item.img+"' class='roundPhoto' /><br /><p><a href='d_groups.html#"+ui.item.href+"'>Visit Profile</a></p>");
		}
		else if (ui.item.category == 'Department'){
			$('#lookupEmpResult').append("<h5>"+ui.item.value+"</h5><img src='images/"+ui.item.img+"' class='roundPhoto' /><br /><p><a href='d_depts.html#"+ui.item.href+"'>Visit Profile</a></p>");
		}
	});

	$('.TotalBtn').click(function(){
		$('.TotalBtn').removeClass('active');
		$(this).addClass('active');
		if($(".TotalBtn:first").hasClass('active')){
			$('.aboutMeBudgetTotal').hide();
			$('.aboutMeRewardsTotal').show();
			return false;
		}
		else{
			$('.aboutMeRewardsTotal').hide();
			$('.aboutMeBudgetTotal').show();
			return false;
		}
		});
	});