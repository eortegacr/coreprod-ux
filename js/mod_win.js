

var multiarraya = [];
var valuea = "";

function logMultiSelecta( selection, img, category, href ) {
	if(multiarraya.indexOf(selection) == -1){
		multiarraya.push(selection);
		var acSelections = "<li class='selection'><a href='" + href + "'><img src='images/" + img + "'>" + selection + "</a><a href='#' class='close'>X</a>";
		$(acSelections).prependTo( "#win_ACSelections" );
	}

// when multselect close is clicked, remove this instance from the array. 
	$("li.selection a").click(function(){
		selection = $(this).parent();
		text = selection.text().slice(0,-1);
		i = multiarraya.indexOf(text);
		if(i != -1){
			multiarraya.splice(i, 1);	
		}
		selection.remove();
	});
}



$(document).ready(function(){

	winStory();

	$("#win button[type='submit']").click(function(){
		winEmps = [];
		$.each($('#win_ACSelections .selection'), function() {
			winEmps.push($("a:first-child", this).html());
		});
		$("input[name='winEmpsList']").val(winEmps);
	});
});

function winStory() {	
// Locate and select employees to reward
	$( ".win_EmpLookup" ).autocomplete({
		source: companyDirectory,
		minLength: 0,
		select: function( event, ui ) {
			logMultiSelecta( ui.item.value, ui.item.img, ui.item.category, ui.item.href );
			$(this).val("");
			return false;
		}
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ){
		return $( "<li>" )
		.data('ui-autocomplete-item', item)
		.append("<a><img src='images/" + item.img + "'>" + item.value + " | " + item.category + "</a>" )
		.appendTo( ul );
	};
};