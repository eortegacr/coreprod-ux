//sample of Selected Vendors
checkoutVendors= {
	100: {
		vendorname:"Eddie Bauer",
		vendorImg:"vendorimages_EddieBauer.png",
		method:"methodPref",
		denom:20,
		qty:1
	},
	113: {
		vendorname:"Foot Locker",
		vendorImg:"vendorimages_FootLocker.png",
		method:"methodPrint",
		denom:100,
		qty:1
	},
	163: {
		vendorname:"Macy's",
		vendorImg:"vendorimages_Macys.png",
		method:"methodPlastic",
		denom:50,
		qty:1
	},
	220: {
		vendorname:"Sears",
		vendorImg:"vendorimages_Sears.png",
		method:"methodPrint",
		denom:100,
		qty:2
	},
	282: {
		vendorname:"Amazon",
		vendorImg:"vendorimages_Amazon.png",
		method:"methodPlastic",
		denom:50,
		qty:3
	}
}

//sample of Rewards
myRewards = {
	4321123443211234: {
		date:"Nov 5, 2013",
		value: 50
	},
	7890678956785678: {
		date:"Feb 12, 2014",
		value: 25
	},
	2468135724681357: {
		date:"Feb 20, 2014",
		value: 1000
	},
	1029384756102938: {
		date:"Mar 18, 2014",
		value: 250
	},
	9687857674656354: {
		date:"Jun 12, 2014",
		value: 50
	},
	8079685746352413: {
		date:"Nov 5, 2014",
		value: 250
	},
	8064975386427531: {
		date:"Jan 7, 2015",
		value: 100
	},
	1111111111111111: {
		date:"Jan 17, 2015",
		value: 100
	}
}

//default variables for checkout calculations like "totals", "rewards value", "rewards applied" and "balance"
subtotals = [];
total = 0;
rewards = 0;
rewardsApplied = 0;
myBalance = 0; 

$(window).load(function() {

	now = new Date();
	add10 = new Date();
	add28 = new Date();

//offset date by 10 days, then format date
	add10.setDate(now.getDate()+10);
	add10days =	$.datepicker.formatDate('M d, yy', add10);

//offset date by 28 days, then format date
	add28.setDate(now.getDate()+28);
	add28days =	$.datepicker.formatDate('M d, yy', add28);

//Loop through vendors to dynamically generate the "Your Order" table. multiply denom and qty for each vendor and push the value to empty array. Set delivery expactation in "Your Order" table depending on method. Append a row to "Your Order" table for each vendor in "checkoutVendors"

	$.each(checkoutVendors, function(index, value) {
		subtotals.push(value.qty * value.denom);
		if(value.method == "methodPrint"){
			checkoutVendorMethod = "On Demand Gift Card";
			deliveryNote = "Redeems instantly";
		}
		if(value.method == "methodPref"){
			checkoutVendorMethod = "Multi-Store Gift Card";
			deliveryNote = "Ships between 2-4 weeks";
		}
		if(value.method == "methodPlastic"){
			checkoutVendorMethod = "Single-Store Gift Card";
			deliveryNote = "Ships between 7-10 days";
		}
		$('.yourOrderTable').append('<tr class="yourOrderVendorRow"><td class="yourOrderVendorImage desktoponly"><img class="largeVendor" src="images/vendors/'+value.vendorImg+'"></td><td class="yourOrderVendorDesc"><h5>'+value.vendorname+'</h5><p>'+checkoutVendorMethod+'</p></td><td class="yourOrderVendorQty"><span class="checkoutQty">'+value.qty+'</span><div class="updateQtySubForm"><input type="text" value="'+value.qty+'" class="updateQtyInput numberOnly" /><br /><input type="button" value="Update" class="updateQtybtn" /></div></td><td  class="yourOrderVendorDenom"><sup>$</sup><span class="checkoutDenom">'+value.denom+'</span></td><td><a href="#" class="checkoutChangeQty">[change qty]</a>&nbsp;<a href="#"  class="checkoutRemoveItem">[delete]</a><br /><span class="deliveryShipping">'+deliveryNote+'</span></td></tr>');
	});

	//Add all subtotals to create Total
	$.each(subtotals,function() {
		total += this;
	});

//Adds all rewards to get my rewards total
	$.each(myRewards, function(index, value) {
		$(value.value).each(function() {
			rewards += this;
		});
	});

	$('.myRewards').append(rewards);

//calculate the total against the rewards and set conditions
	function cartCalc() {
		//Variable to check each reward against. Conditions set below for either balance owned reaching 0, or having a remaining balance.
		balanceowed = total;
		//Loop through rewards to calculate "Balance" and create rows for "Your Payment" table
		$.each(myRewards, function(index, value) {
			//calculate remaining rewards when subracted by balance owed
			rewardRemaining = (value.value - balanceowed);
			//calculate remaining balance by the reward and reset the balance owed for the next iteration of the loop (ex. 100-25=75, 75-25=50, 50-25=25, etc)
			balanceowed = (balanceowed - value.value);
			//append dashes into reward numbers for display on the table (ex. 1111222233334444 >> 1111-2222-3333-4444)
			rewardsWithDashes = index.substring(0,4)+"-"+index.substring(4,8)+"-"+index.substring(8,12)+"-"+index.substring(12,16);
			//If "Balance Owed" is greater than zero, add the reward to "Payment" table with remaining reward value of 0
			if(balanceowed > 0){
				$('.yourPaymentsTable').append('<tr class="rewardLine"><td>'+value.date+'</td><td>'+rewardsWithDashes+'</td><td>$<span class="digits">'+value.value+'</td><td>$<span class="digits">0</td><td><h3 style="color:#8c4;">&#10003</h3></td></tr>');	
			}
			//if "Balance Owed" is equal or less than zero, add the reward to "Payment" table with remaining reward value and stop the loop to prevent additional rewards from displaying
			else{
				$('.yourPaymentsTable').append('<tr class="rewardLine"><td>'+value.date+'</td><td>'+rewardsWithDashes+'</td><td>$<span class="digits">'+value.value+'</td><td>$<span class="digits">'+rewardRemaining+'</td><td><h3 style="color:#8c4;">&#10003</h3></td></tr>');
				return false;
			}
		});

		//subtract the rewards by the total. then check against it in the following conditions
		transMath = (rewards - total);
		balanceowed = (total - rewards);
		//if their are rewards remaining then set the rewards applied value to the total, the balance to a message letting the user know then can complete the checkout, set the charge in the add value form to zero, hide the add value link in step 2, and show the next step in step 2
		if(transMath > 0){	
			rewardsApplied = total;
			myBalance = '<h5 class="alignCenter">Clear to proceed to checkout</span>';
			$('.addValue input[name*="charge"]').val(0);
			$('.addValueToggle').hide();
			$('.paymentNextPrevBtn .nextstep').removeClass('nextStepHide');
		}
		//else set the rewards applied to the rewards total, since the user will need all the rewards to pay for the order, then apply the difference in the balance and the charge input in the add value form, show the add value link and hide the next step in step 2.
		else{
			rewardsApplied = rewards;
			myBalance = '<h3 class="alignCenter"> Balance </h3><h2 class="alignCenter"><a href="#" class="addValueBtn"><sup>$</sup><span class="digits">'+balanceowed+'</span></a>';
			$('.addValue input[name*="charge"]').val(balanceowed);
			$('.addValueToggle').show();
			$('.paymentNextPrevBtn .nextstep').addClass('nextStepHide');	
		}
		//apply the calculated values in elements with specific classes for the user to see
		
		$('.orderTotal').append(total);
		$('.myRewardsApplied').append(rewardsApplied);
		$('.myBalance').append(myBalance);
		$('.remainingRewards').append(transMath);
		//hide row for cc charge in payment table since thats triggered to display when add value form is submitted
		$('.orderCharged').closest('tr').hide();
		//apply commas to numbers in the "digits class"
		$(".digits").digits();
	}

	//run calculations on page load
	cartCalc();

	function cartReset() {
		//Reset variables to accept new values
		subtotals.length = 0;
		total = 0;
		
		//loop through table, multiple each denom to the qty and pass the value to the now empty subtotal array
		$('.yourOrderTable tr').each(function() {
			checkoutQty = $('.checkoutQty', this).text();
			checkoutDenom = $('.checkoutDenom', this).text();
			subtotals.push(checkoutQty * checkoutDenom);
		});
		
		//add all values in the subtotal array to get the new value for the "total"
		$.each(subtotals,function() {
			total += this;
		});

		//reset all html elements where checkout calculations are displayed in preperation to receive new values
		$('.orderTotal, .myRewardsApplied , .myBalance, .remainingRewards, .orderCharged').empty();

		//reset the 'yourPayments' table
		$('.rewardLine').remove();
		$('.chargeLine').remove();
	}

	//when the delete is clicked on the "yourOrder" Table...
	$('.checkoutRemoveItem').click(function(){
		//Remove the product row from the table
		$(this).closest('tr').remove();

		//run reset of values and tables
		cartReset();

		//rerun calculations
		cartCalc();

	});

	//when change qty button is clicked, toggle display of "qty" with mini "update qty" input
	$('.checkoutChangeQty').click(function(){
		$(this).parent().siblings('td:has(.checkoutQty)').children('.checkoutQty').fadeOut('fast');
		$(this).parent().siblings('td:has(.updateQtySubForm)').children('.updateQtySubForm').fadeIn('fast');
	});

	//when change qty is clicked, do the following...
	$('.updateQtybtn').click(function(){
		//if the value entered is zero, remove the table row
		if ($(this).siblings('.updateQtyInput').val() == 0){
			$(this).closest('tr').remove();
		}

		//toggle display of "qty" and "update qty" input AND update "qty" value with what was enetered
		$(this).parent().siblings('.checkoutQty').empty();
		$(this).parent().siblings('.checkoutQty').append($(this).siblings('.updateQtyInput').val());
		$(this).parent().fadeOut('fast');
		$(this).parent().siblings('.checkoutQty').fadeIn('fast');

		//run reset of values and tables
		cartReset();

		//rerun calculations
		cartCalc();
	});

	//click next on shipping table, then pass value to place order and receipt page
	$('.shippingSubmit').click(function(){
		var sfname	= $('#checkout3 input[name*="first_name"]').val();
		var slname	= $('#checkout3 input[name*="last_name"]').val();
		var semail	= $('#checkout3 input[name*="email"]').val();
		var sadd1	= $('#checkout3 input[name*="address1"]').val();
		var sadd2	= $('#checkout3 input[name*="address2"]').val();
		var scity	= $('#checkout3 input[name*="city"]').val();
		var sstate	= $('#checkout3 select[name*="state"]').val();
		var szip	= $('#checkout3 input[name*="zip"]').val();
		var scountry	= $('#checkout3 select[name*="country"]').val();
		//reset classes "shippingAddress" in prep of new append
		$('.shippingAddress').empty();
		//condition if no address 2 value. removes a line break
		if(!sadd2){
			$('.shippingAddress').append(sfname+' '+slname+'<br />'+semail+'<br />'+sadd1+'<br />'+scity+' '+sstate+' '+szip+'<br />'+scountry);
		}

		//condition if address 2 has a value
		else{
			$('.shippingAddress').append(sfname+' '+slname+'<br />'+semail+'<br />'+sadd1+'<br />'+sadd2+'<br />'+scity+' '+sstate+' '+szip+'<br />'+scountry);
		}
	});

	//when click the next button on the "Your order" table, pass the vendor info to the place order and receipt page 
	$('#checkout .nextstep').click(function(){
		//empty the place order vendor table, then append vendor info
		$('.reviewedVendors').remove();
		$('.yourOrderVendorRow').each(function() {
			yourOrderVendorImage = $('.yourOrderVendorImage', this).html();
			yourOrderVendorDesc = $('.yourOrderVendorDesc', this).html();
			yourOrderVendorQty = $('.yourOrderVendorQty > .checkoutQty', this).html();
			yourOrderVendorDenom = $('.yourOrderVendorDenom', this).html();
			
			//for each method value append a delivery date from valiables set above or print now button
			if($('.deliveryShipping', this).html() == 'Ships between 2-4 weeks'){
				deliveryBy = 'Expected delivery date:<h6>'+add28days+'</h6>';
				}
			if($('.deliveryShipping', this).html() == 'Ships between 7-10 days'){
				deliveryBy = 'Expected delivery date:<h6>'+add10days+'</h6>';
			}
			if($('.deliveryShipping', this).html() == 'Redeems instantly'){
				deliveryBy = '<a class="btn" href="#">Print Now</a><br><br><h6>This has been emailed<br>to you as well</h6>';
			}

			//Append vendor data to both the place order table and the receipts page
			$('.yourReviewTableHeaders').after('<tr class="reviewedVendors"><td class="desktoponly">'+yourOrderVendorImage+'</td><td>'+yourOrderVendorDesc+'</td><td>'+yourOrderVendorQty+'</td><td>'+yourOrderVendorDenom+'</td></tr>');	
			$('.yourRecieptTableHeaders').after('<tr class="reviewedVendors"><td class="desktoponly">'+yourOrderVendorImage+'</td><td>'+yourOrderVendorDesc+'</td><td>'+yourOrderVendorQty+'</td><td>'+yourOrderVendorDenom+'</td><td>'+deliveryBy+'</td></tr>');
		});
	});

	//after place order button is clicked, go to the receipt page
	$('.checkoutSubmit').click(function(){
		$('#checkout4').removeClass('current');
		$('#checkout5').addClass('current');
		return false;
	});

	$("#addValueForm").submit(function(event){ 
		if($(this).valid()){
		$('.mainOverlay').slideUp(500);
		$('.mainOverlay > div').fadeOut(500);

		//reset and append charge value
		charge = 0;
		charge = parseInt($('.addValue input[name*="charge"]').val());

		//format chage info for display
		ccnumber = "xxxxxxxxxxxx"+$('.addValue input[name*="CCNumber"]').val().substring(12,16);
		todaysDate = $.datepicker.formatDate('M d, yy', new Date());

		//reset and recalculate all values in the checkout. This is the same as previously calculated with a few differences...
		subtotals.length = 0;
		total = 0;
		$('.yourOrderTable tr').each(function() {
			checkoutQty = $('.checkoutQty', this).text();
			checkoutDenom = $('.checkoutDenom', this).text();
			subtotals.push(checkoutQty * checkoutDenom);
		});

		$.each(subtotals,function() {
			total += this;
		});

		$('.orderTotal, .myRewardsApplied , .myBalance, .remainingRewards, .orderCharged').empty();
		$('.rewardLine').remove();
		$('.chargeLine').remove();
		balanceowed = total;
		$.each(myRewards, function(index, value) {
			rewardRemaining = (value.value - balanceowed);
			balanceowed = (balanceowed - value.value);
			rewardsWithDashes = index.substring(0,4)+"-"+index.substring(4,8)+"-"+index.substring(8,12)+"-"+index.substring(12,16);
		if(balanceowed > 0){
			$('.yourPaymentsTable').append('<tr class="rewardLine"><td>'+value.date+'</td><td>'+rewardsWithDashes+'</td><td>$<span class="digits">'+value.value+'</td><td>$<span class="digits">0</td><td><h3 style="color:#8c4;">&#10003</h3></td></tr>');	
		}
		else{
			$('.yourPaymentsTable').append('<tr class="rewardLine"><td>'+value.date+'</td><td>'+rewardsWithDashes+'</td><td>$<span class="digits">'+value.value+'</td><td>$<span class="digits">'+rewardRemaining+'</td><td><h3 style="color:#8c4;">&#10003</h3></td></tr>');
			return false;
		}
		
	});

//if there is still a balance after the rewards are spend, add the charge and display the cc charge for the user to see  
		if(balanceowed > 0){
			$('.yourPaymentsTable').append('<tr class="chargeLine"><td>'+todaysDate+'</td><td>Credit Card Charge</td><td>$<span class="digits">'+charge+'</td><td>--</td><td><h3 style="color:#8c4;">&#10003</h3></td></tr>');	
		}
		rewardsWithCharge = (rewards + charge);
		transMath = (rewardsWithCharge - total);
		balanceowed = (total - rewardsWithCharge);
		if(transMath >= 0){
			rewardsApplied = total;
			myBalance = '<h5 class="alignCenter">Clear to proceed to checkout</span>';
			$('.addValue input[name*="charge"]').val(0);
			$('.addValueToggle').hide();
			$('.paymentNextPrevBtn .nextstep').removeClass('nextStepHide');
		}else{
			rewardsApplied = rewards;
			myBalance = '<h3 class="alignCenter"> Balance </h3><h2 class="alignCenter"><a href="#" class="addValueBtn"><sup>$</sup><span class="digits">'+balanceowed+'</span></a>';
			$('.addValue input[name*="charge"]').val(balanceowed);
			$('.addValueToggle').show();
			$('.paymentNextPrevBtn .nextstep').addClass('nextStepHide');
		}
		$('.orderTotal').append(total);
		$('.myRewardsApplied').append(rewards);
		$('.myBalance').append(myBalance);
		$('.orderCharged').closest('tr').show();
		$('.orderCharged').append(charge);
		$('.remainingRewards').append(transMath);
		$(".digits").digits();
		}
	});
});