$(window).load(function(){
	$('.formWizardSteps li').eq(0).children('a').addClass('fwStepscurrent');
//Form Wizard, General Functions. Modules may have additional function build off of this base. ie. validation, passing values to review step, etc.
	$('.nextstep').click(function(){
		$(this).parents("form").find("input, textarea, select, button").addClass("ignore");
		$(this).parents("fieldset").find("input.required, textarea.required, select.required, button.required").removeClass("ignore");
		if(!$(this).parents("form").valid()){
			return false;
		}
		else{
		$(this).parents('fieldset').hide().addClass('prevfs');
		$(this).parents('fieldset').next().show();
		$(this).closest('.formWizard').find('.formWizardSteps li a').removeClass('fwStepscurrent');
		fspassed = $(this).parents('fieldset').prevAll().size();
		$(this).closest('.formWizard').find('.formWizardSteps li').each(function() {
			if( $(this).index() < fspassed ){
				$('a',this).addClass('fwStepsprev');
			}
			if( $(this).index() == fspassed ){
				$('a',this).addClass('fwStepscurrent');
			}
		});
		}
	});

	$('.prevstep').click(function(){
		$(this).parents('fieldset').hide();
		$(this).parents('fieldset').prev().show().removeClass('prevfs');
		$(this).closest('.formWizard').find('.formWizardSteps a').removeClass('fwStepscurrent').removeClass('fwStepsprev');
		fspassed = $(this).parents('fieldset').prevAll('.prevfs').size();
		$(this).closest('.formWizard').find('.formWizardSteps li').each(function() {
			if( $(this).index() < fspassed ){
				$('a',this).addClass('fwStepsprev');
			}
			if( $(this).index() == fspassed ){
				$('a',this).addClass('fwStepscurrent');
			}
		});
	});

	$('.formWizardSteps').on('click', '.fwStepsprev', function(){
		fsjumpto = $(this).parents('li').index();
		$(this).parents('.formWizard').find('fieldset').hide();
		$(this).parents('.formWizard').find('fieldset').eq(fsjumpto).show().removeClass('prevfs');
		$(this).parents('.formWizard').find('fieldset').eq(fsjumpto).nextAll().removeClass('prevfs');
		$(this).closest('.formWizard').find('.formWizardSteps a').removeClass('fwStepscurrent').removeClass('fwStepsprev');
		$(this).addClass('fwStepscurrent');
		fspassed = $(this).parent('li').prevAll().size() + 1;
		console.log(fspassed);
		$(this).closest('.formWizard').find('.formWizardSteps li').each(function() {
			if( $(this).index() < fspassed ){
				$('a',this).addClass('fwStepsprev');
			}
		});
	});

	

});