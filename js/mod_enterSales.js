$(window).load(function(){

	$("input[name='sales_Promo']").click(function(){
		descripName=$(this).val();
		$('.sales_PromoDescription').hide();
		$(".sales_PromoDescription:has(h3:contains('"+descripName+"'))").show();
	});

	$('.nextstep').click(function(){
		$('.salesEntry input').removeClass('required');
		if($(this).parents('fieldset').next().hasClass('salesEntryContainer')){
			var selectedPromo = $("input[name='sales_Promo']:checked").parent().index();
			$('.salesEntry').hide();
			$('.salesEntry:eq('+selectedPromo+')').show();
			$('.salesEntry input[type="radio"]').prop('checked', false).removeClass('valid');
			$('.salesEntry .graphic_radio a').removeClass('selected');
			$('.salesEntry:eq('+selectedPromo+') .addreq').addClass('required');
		}
	});
	
	$('.prevstep').click(function(){
		if($(this).parents('fieldset').hasClass('salesEntryContainer')){
			$(this).parents('fieldset').find('.addreq').removeClass('required');
			$('.salesEntry label.error').remove();
		}
	});

});