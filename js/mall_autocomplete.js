//This is for the autocomplete functions in the mall. Autocomplete needs to be isolated and only applied to pages where an autocomplete field exists, or all following scripts will fail
$(document).ready(function () {

//in mall search, the autocomplete searched through all vendors and categories. ex. user enters "bab", autocomplete will return the category babies and kids AND the vendor Baby Gap
	$( ".mallSearch" ).autocomplete({
		source: acObjects,
		minLength: 0,
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ){
		if (item.classification == 'Vendor'){
			return $( "<li>" )
			.data('ui-autocomplete-item', item)
			.append("<a><img class='ACVendorImage' src='images/vendors/" + item.img + "'>" + item.value + " | " + item.classification + "</a>" )
			.appendTo( ul );
			} else {
		return $( "<li>" )
			.data('ui-autocomplete-item', item)
			.append("<a><img src='images/vendors/" + item.img + "'>" + item.value + " | " + item.classification + "</a>" )
			.appendTo( ul );
			}
	};

//when an autocomplete entry is selected, clear out the search results. then...
	$( ".mallSearch" ).on( "autocompleteselect", function( event, ui ) {
		$( '.searchResultsContainer').slideDown('fast');
		$( ".vendorSearchResults > ul > li").remove();
		$(".categorySearchResults > ul >li" ).slideUp('slow');
		$('.searchResultsInfo').empty();
		$('.searchResultsInfo').append("<div class='content'><h5>Search results for: "+ui.item.value+"</h5></div>");

//...loop through all vendors and categories for a match. then...
		$.each(acObjects, function(index, value) {
			selectedID = value.id;
			selectedName = value.value;
			selectedImg = value.img;
			selectedCategory = value.category;
			selectedClassification = value.classification;

//...if a vendor was selected, search through vendors and when a match is found, display the vendor in the search result... 
			if (selectedName == ui.item.value && selectedClassification == "Vendor" ) {
				$(".vendorSearchResults > ul").append("<li class='content'><a href='d_mall_detail.html#"+selectedID+"' class='imagelink vendorSelection'><img src='images/vendors/"+ selectedImg + "' /><br />" + selectedName + "</a></li>");
				$(".vendorSearchResults > ul > li").show();
			}

//...if a category was selected, search through Vendor Categories and Categories and when a match is found, display the vendor in the search result AND display the Category in the search results... 
			if (($.inArray( ui.item.value, selectedCategory) > -1) || (selectedName == ui.item.value) ) {
				if (($.inArray( ui.item.value, selectedCategory) > -1) && (selectedClassification == "Vendor")){
					$(".vendorSearchResults > ul").append("<li class='content'><a href='d_mall_detail.html#"+selectedID+"' class='imagelink vendorSelection'><img src='images/vendors/"+ selectedImg + "' /><br />" + selectedName + "</a></li>");
					$(".vendorSearchResults > ul > li").show();
				}
				if ((selectedName == ui.item.value) && (selectedClassification == "Category")){
					$(".categorySearchResults > ul > li:has(a:contains("+selectedName+"))").slideDown("slow");
				}
			}
		});
	});

//in the Shop by Vendor autocomplete, do the same as about only checking and matching vendors 
	$( ".vendorFilter" ).autocomplete({
		source: acVendors,
		minLength: 0,
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ){
		return $( "<li>" )
		.data('ui-autocomplete-item', item)
		.append("<a><img class='ACVendorImage' src='images/vendors/" + item.img + "'>" + item.value + "</a>" )
		.appendTo( ul );
	};
	$(".vendorFilter").on( "autocompleteselect", function( event, ui ) {
		$( '.searchResultsContainer').slideDown('fast');
		$( ".vendorSearchResults > ul > li").remove();
		$(".categorySearchResults > ul >li").slideUp('slow');
		$('.searchResultsInfo').empty();
		$('.searchResultsInfo').append("<div class='content'><h5>Search results for: "+ui.item.value+"</h5></div>");
		$('html,body').animate({
			scrollTop: $('.mallContainer').offset().top - 70
		});
		$.each(acObjects, function(index, value) {
			selectedID = value.id;
			selectedName = value.value;
			selectedImg = value.img;
			selectedCategory = value.category;
			selectedClassification = value.classification;
			if (selectedName == ui.item.value ) {
				$( ".vendorSearchResults > ul" ).append("<li class='content'><a href='d_mall_detail.html#"+selectedID+"' class='imagelink vendorSelection'><img src='images/vendors/"+ selectedImg + "' /><br />" + selectedName + "</a></li>");
				$( ".vendorSearchResults > ul > li" ).show();
			}
		});
	});

});