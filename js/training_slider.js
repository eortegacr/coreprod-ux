sliderInt=1;
sliderNext=2;

function startSlider(){
	countOfTrainingSlides = $("#trainingDoc > Div").size();
}

function showSlide(id){
	if(id > countOfTrainingSlides){
		id=countOfTrainingSlides;}
	else if(id < 1) {
		id = 1;}
	$("#trainingDoc > div").hide();
	$("#trainingDoc > div#trainingSlide" + id).fadeIn(200);
	$('.pagination > a').removeClass('pagactive');
	$('.pagination > a:eq('+(id-1)+')').addClass('pagactive');
	sliderInt = id;
	sliderNext = id +1;
}

function prev(){
	newSlide = sliderInt - 1;
	showSlide(newSlide);
}

function next(){
	newSlide = sliderInt + 1;
	showSlide(newSlide);
}

$(document).ready(function(){

	$("#trainingDoc > div#trainingSlide1").fadeIn(200);
	$('.pagination > a').removeClass('pagactive');
	$('.pagination > a:eq(0)').addClass('pagactive');
	startSlider();

});