$(window).load(function(){

	$('.activeContent').click(function(){
		if($(this).text() == "disable"){
			$(this).text('enable');
		}
		else{
			$(this).text('disable')
		}
	});

	$('.remove').click(function(){
		$(this).parents('li').remove();
	});

	$(function() {
		$( "#sortable" ).sortable();
		$( "#sortable" ).disableSelection();
	});
	
	$("#heroOrder input[type='submit']").click(function(){
		$("input[name='new hero order']").val("");
		heroOrder = [];
		$.each($('.heroVal'), function() {
			heroOrder.push($(this).html());
		});
		$("input[name='new hero order']").val(heroOrder);
	});
	
});