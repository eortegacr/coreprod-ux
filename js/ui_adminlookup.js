	

var multiarrayadmin = [];
var valueadmin = "";

function logMultiSelectadmin( selection, img, category, href ) {
	if(multiarrayadmin.indexOf(selection) == -1){
		multiarrayadmin.push(selection);
		var acSelections = "<li class='selection'><img src='images/" + img + "'>" + selection + "<a href='#' class='close'>X</a>";
		$(acSelections).prependTo( "#admin_ACSelections" );
	}

// when multselect close is clicked, remove this instance from the array. 
	$("li.selection a").click(function(){
		selection = $(this).parent();
		text = selection.text().slice(0,-1);
		i = multiarrayadmin.indexOf(text);
		if(i != -1){
			multiarrayadmin.splice(i, 1);	
		}
		selection.remove();
	});
}

$(window).load(function(){

adminLookup();

	$("form:has('.adminLookup') button[type='submit']").click(function(){
		adminLookup = [];
		$.each($('#admin_ACSelections .selection'), function() {
			adminLookup.push($(this).html());
		});
		$("input[name='adminLookupList']").val(adminLookup);
	});

});

function adminLookup() {
	
	// Locate and select employees to reward
	$( ".adminLookup" ).autocomplete({
		source: companyDirectory,
		minLength: 0,
		select: function( event, ui ) {
			logMultiSelectadmin( ui.item.value, ui.item.img, ui.item.category, ui.item.href );
			$(this).val("");
			return false;
		}
	})

	.data( "ui-autocomplete" )._renderItem = function( ul, item ){
			return $( "<li>" )
			.data('ui-autocomplete-item', item)
			.append("<a><img src='images/" + item.img + "'>" + item.value + " | " + item.category + "</a>" )
			.appendTo( ul );
	};


};