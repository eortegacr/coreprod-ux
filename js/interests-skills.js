interests = [
	{
		value: "Acting",
		img: "profileIcons/interestActing.png",
		category: "Interest"
	},
	{
		value: "Archery",
		img: "profileIcons/interestArchery.png",
		category: "Interest"
	},
	{
		value: "Art",
		img: "profileIcons/interestArt.png",
		category: "Interest"
	},
	{
		value: "Basketball",
		img: "profileIcons/interestBasketball.png",
		category: "Interest"
	},
],
	
skills = [
	{
		value: "Accounting",
		img: "profileIcons/skillAccounting.png",
		category: "Skill"
	},
	{
		value: "Administrative",
		img: "profileIcons/skillAdmin.png",
		category: "Skill"
	},
	{
		value: "Analytics",
		img: "profileIcons/skillAnalytics.png",
		category: "Skill"
	},
	{
		value: "Software",
		img: "profileIcons/skillSoftware.png",
		category: "Skill"
	},
];

$(document).ready(function(){
	
var multiarrayInt = [];
var valueInt = "";

var multiarray = [];
var value = "";

// INTERESTS LOOKUP

//Check for duplicates. load each selection into an array. when clicking to advance to next step, check array. If selection is NOT in array, add it and display the choice to the user. If not return.
function logIntSelect( selection, img, category ) {
	if(multiarrayInt.indexOf(selection) == -1){
		multiarrayInt.push(selection);
		var interestSelections = "<li class='int-selection'><a href='#' class='profileIcon interestIcon'><img src='images/" + img + "'></a><p>" + selection + "</p><a href='#' class='close'>X</a></li>";
		$(interestSelections).prependTo( '#interest_selections' );
	}

	// when multselect close is clicked, remove this instance from the array.
	$("li.int-selection a.close").click(function(){
  		selection = $(this).parent();
		text = selection.text().slice(0,-1);
		i = multiarrayInt.indexOf(text);
		if(i != -1){
			multiarrayInt.splice(i, 1);
		}
		selection.remove();
		return false;
	});
}



	$( ".filterInterestsLookup" ).autocomplete({
		source: interests,
		minLength: 0,
		select: function( event, ui ) {
			logIntSelect( ui.item.value, ui.item.img, ui.item.category );
			$(this).val("");
			return false;
		}
	})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ){
		return $( "<li>" )
		.data('ui-autocomplete-item', item)
		.append("<a><img src='images/" + item.img + "'>" + item.value + " | " + item.category + "</a>" )
		.appendTo( ul );
		
	};


// SKILLS LOOKUP
function logMultiSelect( selection, img, category ) {
	if(multiarray.indexOf(selection) == -1){
		multiarray.push(selection);
		var skillsSelections = "<li class='int-selection'><a href='#' class='profileIcon interestIcon'><img src='images/" + img + "'></a><p>" + selection + "</p><a href='#' class='close'>X</a></li>";
		$(skillsSelections).prependTo( '#skills_selections' );
	}

	// when multselect close is clicked, remove this instance from the array.
	$("li.int-selection a.close").click(function(){
  		selection = $(this).parent();
		text = selection.text().slice(0,-1);
		i = multiarray.indexOf(text);
		if(i != -1){
			multiarray.splice(i, 1);
		}
		selection.remove();
		return false;
	});
}

$( ".filterSkillsLookup" ).autocomplete({
		source: skills,
		minLength: 0,
		select: function( event, ui ) {
			logMultiSelect( ui.item.value, ui.item.img, ui.item.category );
			$(this).val("");
			return false;
		}
	})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ){
		return $( "<li>" )
		.data('ui-autocomplete-item', item)
		.append("<a><img src='images/" + item.img + "'>" + item.value + " | " + item.category + "</a>" )
		.appendTo( ul );
		
	};

	$('a.close').click(function() {
		$(this).parents('li').remove();
		return false;
	});

});


