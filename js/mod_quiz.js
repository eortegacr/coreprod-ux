$(document).ready(function() {

	setInterval(function(){
		var beginDate = new Date("3/21/2015");
		var todayDate = new Date();
	
		if(todayDate < beginDate){
			var todayHrs = (23)-(todayDate.getHours());
			var todayMin = (59)-(todayDate.getMinutes());
			var todaySec = (59)-(todayDate.getSeconds());
	
			var timeDiff = Math.abs(todayDate.getTime() - beginDate.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	
			$('.durationDay').text(diffDays-1);
			$('.durationHr').text(todayHrs);
			$('.durationMin').text(todayMin);
			$('.durationSec').text(todaySec);
		}
	}, 1000);

//START NEXT/PREV FUNCTIONALITY
	countQuizQ = $('.quizQuestion').size();
	currentQuizQ = 1;
	nextQuizQ = 2;
	prevQuizQ = countQuizQ;
	$('.prevQuizQ, .quizControlBreaker').hide();

	$('.nextQuizQ').click(function () {
		if($('#quizSlide' + currentQuizQ+' input:checked').length){
			correctAns= $('#quizSlide' + currentQuizQ+' .quizRightAns').size();
			checkedCorrectAnswer = $('#quizSlide' + currentQuizQ+' .quizRightAns:checked').size();
			if($('#quizSlide' + currentQuizQ+' .quizWrongAns:checked').length || checkedCorrectAnswer < correctAns){
				$('.quizLife:last').removeClass('quizLife');
			}
			$('#quizSlide' + currentQuizQ).hide();
			$('#quizSlide' + nextQuizQ).fadeIn('slow');
			prevQuizQ = nextQuizQ - 1;
			currentQuizQ = nextQuizQ;
			nextQuizQ = nextQuizQ + 1;
			$('.pageQuizQ').empty();
			$('.pageQuizQ').text(currentQuizQ);
			console.log($('.quizLife').length);
			if (nextQuizQ > (countQuizQ + 1)){
				$('#mainQuiz').hide();
				$('.quizCompleteConfirm').show();
				if($('.quizLife').length){
					$('.quizSuccess').show();
					$('.quizFail').hide();
				}
				else {
					$('.quizSuccess').hide();
					$('.quizFail').show();
				}
			}
			if (currentQuizQ == 0){
				currentQuizQ = countQuizQ;
			}
			return false;
		}
	});

	$('.pageQuizQ').empty();
	$('.pageQuizQ').text(currentQuizQ);

	var total = $('.sizeQuizQ');
	total.text(countQuizQ);
});