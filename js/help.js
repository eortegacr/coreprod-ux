// OPEN LINK IN NEW WINDOW - SCROLL TO AND OPEN SECTION
$(window).load(function () {
	section = window.location.hash.substring(1);
	if($(section) !== null){
		$('html,body').animate({
			scrollTop: $('#' + section).offset().top - 70
		});
		$('#' + section + '.collapseBtn').trigger('click');
	}

});